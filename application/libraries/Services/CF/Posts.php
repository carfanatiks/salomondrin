<?php
class Services_CF_Posts
{

	
	public $testing = '1';


	/**
	 * The Mobile constructor
	 */
	public function __construct() {


		$this->ci =& get_instance();

	}

	public function getPostTypeData($type){
		$this->ci->load->model('Postsmain_model');
		$type = $this->ci->Postsmain_model->getPostTypeData($type);
		return $type;
	}
	
	public function getMediaUrl($table, $id){
		$this->ci->load->model('Postsmain_model');
		$url = $this->ci->Postsmain_model->getMediaUrl($table,$id);
		return $url;
	}

}


