<?php
class Services_CF_Hashtags{

	/**
	 * The Mobile constructor
	 */
	public function __construct() {


		$this->ci =& get_instance();

	}


	function getHashtags($string) {  
	    $hashtags= FALSE;  
	    preg_match_all("/(#\w+)/u", $string, $matches);  
	    if ($matches) {
	        $hashtagsArray = array_count_values($matches[0]);
	        $hashtags = array_keys($hashtagsArray);
	    }
	    return $hashtags;
	}


	function getMentions($string) {  
	    $hashtags= FALSE;  
	    preg_match_all("/(@\w+)/u", $string, $matches);  
	    if ($matches) {
	        $hashtagsArray = array_count_values($matches[0]);
	        $hashtags = array_keys($hashtagsArray);
	    }
	    return $hashtags;
	}

	function convert_comment_links($comment, $urlMention, $urlTag )
	{
		//converts URLs to active links
		$comment = preg_replace('/((http)+(s)?:\/\/[^<>\s]+)/i', '<a href="$0" target="_blank">$0</a>', $comment );

		//converts mentions (e.g. @stathisg) to active links, pointing to the user's twitter profile
		$comment = preg_replace('/[@]+([A-Za-z0-9-_.]+)/', '<span class="tagcomment"><a href="'.$urlMention.'/$1" target="_self">$1</a></span>', $comment );

		//converts hashtags (e.g. #test) to active links, pointing to a twitter's search URL
		$comment = preg_replace('/[#]+([A-Za-z0-9-_.]+)/', '<span class="tagcomment"><a href="'.$urlTag.'/$1" target="_self">$0</a></span>', $comment );

		return $comment;
	}

}