<?php
class Services_CF_Maker
{

	
	public $testing = '1';


	/**
	 * The Mobile constructor
	 */
	public function __construct() {


		$this->ci =& get_instance();

	}

	public function getMaker(){
		$this->ci->load->model('Carmaker_model');
		$makers = $this->ci->Carmaker_model->getAllMaker();
		return $makers;
	}
	public function getCategories(){
		$this->ci->load->model('Carmaker_model');
		$categories = $this->ci->Carmaker_model->getAllCategories();
		return $categories;
	}
	public function getModel($id){
		$this->ci->load->model('Carmaker_model');
		$models = $this->ci->Carmaker_model->getModelId($id);
		return $models;
	}
		public function getYearModel($id){
		$this->ci->load->model('Carmaker_model');
		$makersyear = $this->ci->Carmaker_model->getYearModel($id);
		return $makersyear;
	}


}


