<?php

class Services_CF_CDNMakerProfile
{

	
	public $testing = '1';
	public $container  = 'cf_thumbs';


	/**
	 * The Mobile constructor
	 */
	public function __construct() {


		$this->ci =& get_instance();

        $this->ci->load->helper('path');
        $this->ci->load->library('cf/cfiles');
       // $this->ci->cfiles->cf_container =  $this->container;

	}

	public function getURLObject($file_name){
		$this->testing = '2';
		if($container_info = $this->ci->cfiles->container_info())
        {
        	$url = $container_info->cdn_uri;
            //echo '<p>'.$container_info->name.'</p>';
            //echo '<p>'.$container_info->cdn_uri.'</p>';
        }
        else
        {
            die('Container Invalid');
        }
        $url = $url.$file_name;
		return $url;
	}
		

	public function getURL(){
		/*if($container_info = $this->ci->cfiles->container_info())
        {
        	$url = $container_info->cdn_uri;
            //echo '<p>'.$container_info->name.'</p>';
            //echo '<p>'.$container_info->cdn_uri.'</p>';
        }
        else
        {
            die('Container Invalid');
        }*/
        $url = 'http://53d8e8de4b1b3db135de-108d1a1b44db6dc6881e0aad90e83b96.r78.cf2.rackcdn.com';
		return $url;
	}
    public function getURLLogos(){
        /*if($container_info = $this->ci->cfiles->container_info())
        {
            $url = $container_info->cdn_uri;
            //echo '<p>'.$container_info->name.'</p>';
            //echo '<p>'.$container_info->cdn_uri.'</p>';
        }
        else
        {
            die('Container Invalid');
        }*/
        $url = 'http://c2513008a33cd5281f4f-fa251928a8053f07b16d69deaa053273.r23.cf2.rackcdn.com';
        return $url;
    }
    public function getModelThumbs(){
        /*if($container_info = $this->ci->cfiles->container_info())
        {
            $url = $container_info->cdn_uri;
            //echo '<p>'.$container_info->name.'</p>';
            //echo '<p>'.$container_info->cdn_uri.'</p>';
        }
        else
        {
            die('Container Invalid');
        }*/
        $url = 'http://0346937d45ecc992def6-df836780ee1c803abc81c688fdb575bf.r55.cf2.rackcdn.com';
        return $url;
    }
    public function getLogoThumbs(){
        /*if($container_info = $this->ci->cfiles->container_info())
        {
            $url = $container_info->cdn_uri;
            //echo '<p>'.$container_info->name.'</p>';
            //echo '<p>'.$container_info->cdn_uri.'</p>';
        }
        else
        {
            die('Container Invalid');
        }*/
        $url = 'http://fe072449c35a9c936701-435e8ef2ee4b231aef80141e93f60470.r20.cf2.rackcdn.com';
        return $url;
    }
    

    public function getOriginalImage(){
        $url = 'http://270c6e8f7cbcda346fe7-f8f1ba0c82851349ec68a411d92a0955.r61.cf2.rackcdn.com';
        return $url;
        
    }

    public function getMainCDN(){
        /*if($container_info = $this->ci->cfiles->container_info())
        {
            $url = $container_info->cdn_uri;
            //echo '<p>'.$container_info->name.'</p>';
            //echo '<p>'.$container_info->cdn_uri.'</p>';
        }
        else
        {
            die('Container Invalid');
        }*/
        $url = 'http://d2e650b2aa60e5e09148-5510baaaf7b9c67d555a7ca31f4aed86.r30.cf2.rackcdn.com';
        return $url;
    }
    public function getMobileCDN(){
        /*if($container_info = $this->ci->cfiles->container_info())
        {
            $url = $container_info->cdn_uri;
            //echo '<p>'.$container_info->name.'</p>';
            //echo '<p>'.$container_info->cdn_uri.'</p>';
        }
        else
        {
            die('Container Invalid');
        }*/
        $url = 'http://d1d7e83dd18d03024da5-6579ef533e19a304381a74cf513cefc0.r80.cf2.rackcdn.com';
        return $url;
    }
    public function getThumbCDN(){
        /*if($container_info = $this->ci->cfiles->container_info())
        {
            $url = $container_info->cdn_uri;
            //echo '<p>'.$container_info->name.'</p>';
            //echo '<p>'.$container_info->cdn_uri.'</p>';
        }
        else
        {
            die('Container Invalid');
        }*/
        $url = 'http://8c714ff1d723dfc50cea-012943c9b994053101a14674af4c10a4.r96.cf2.rackcdn.com';
        return $url;
    }
    public function getMobileThumbCDN(){
        /*if($container_info = $this->ci->cfiles->container_info())
        {
            $url = $container_info->cdn_uri;
            //echo '<p>'.$container_info->name.'</p>';
            //echo '<p>'.$container_info->cdn_uri.'</p>';
        }
        else
        {
            die('Container Invalid');
        }*/
        $url = 'http://27589251ae1747748862-bb4b3bc82f39f60f785f6b5c4a6695fd.r33.cf2.rackcdn.com';
        return $url;
    }


    public function getChallengeMedia(){
        $url = 'http://122b8dab82e843612dfb-7e2f038f391fdcc5a01f14dc66ed1add.r81.cf2.rackcdn.com';
        return $url;
    }
    public function getChallengeMobileMedia(){
        $url = 'http://fa06bc42dfbbe05028a6-23cddff92dca568141f50ce3f79f0be0.r44.cf2.rackcdn.com';
        return $url;
    }

    public function getChallengeThumb(){
        $url = 'http://872979d3eaba4948d5e1-2bddd815f3cab6faed6f364ef7ff7548.r24.cf2.rackcdn.com';
        return $url;
    }

    public function getUserWallImage(){
        $url = 'http://96eb79f23583d5eba03b-3af84752bcb4e888a5f5d5c0deea4f72.r54.cf2.rackcdn.com';
        return $url;
    }

    public function getUserProfileImage(){
        $url = 'http://9ac8d72b6720ecc473ed-a223fb79f69743ae1a2104c258d978d3.r75.cf2.rackcdn.com';
        return $url;
    }
}



