<?php

class Services_CF_SaveMedia
{

	


	/**
	 * The Mobile constructor
	 */
	public function __construct() {


		$this->ci =& get_instance();

        $this->ci->load->helper('path');
        
        $this->ci->load->library('opencloud');
       // $this->ci->cfiles->cf_container =  $this->container;

	}

	public function SavePreviewImages($file){

        	$file_location = 'images/uploads/';
        

        	$dir =  absolute_path($file_location);
        	// Set the allowed file extensions
        	$fileTypes = array('jpg', 'jpeg', 'gif', 'png', 'bmp', 'txt'); // Allowed file extensions

			$tempFile   = $file['Filedata']['tmp_name'];
            $uploadDir  = $dir;
            $targetFile = $uploadDir . $file['Filedata']['name'];
            $fileParts = pathinfo($file['Filedata']['name']);
            if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
            	$this->ci->benchmark->mark('code_start');

          
                    $rnd = md5(time()).'.jpg';
                    move_uploaded_file($tempFile, $targetFile);
                    $img = new imagick($targetFile);
                    $img->setImageFormat('jpeg');
                    $d = $img->getImageGeometry();
                    $w = $d['width'];
                    if($w > 1280) 
                    $img->scaleImage(1280,0);
                	$d = $img->getImageGeometry();
                    $h = $d['height'];
                    if($h > 720) 
                    $img->scaleImage(0,720);


                    $img->writeImage($dir.$rnd ); //output
                    $this->ci->benchmark->mark('code_end');
                    log_message('debug', 'TIME: RESIZE:'.$this->ci->benchmark->elapsed_time('code_start', 'code_end'));
                    $img->clear();
                    $img->destroy();
                    unlink($targetFile);
                    log_message('debug', 'TIME: IMAGE:'.$targetFile);

                    //$this->ci->cfiles->cf_container = 'cf_preview_image';
                    //$container_info = $this->ci->cfiles->container_info();
                    $url_container = 'http://86839a4a03d6d7304c9d-c1a65037a58eb95f6995e24c723ab9d6.r58.cf1.rackcdn.com/hd';
                    $file = $dir.$rnd;
                    
                    
                    $this->ci->opencloud->set_container('images');
					$this->ci->opencloud->add_object($rnd, file_get_contents($file), 'image/jpeg');
                    log_message('debug', 'TIME: URL:'.$url_container.'/'.$rnd);
                    
                    
                    return $url_container.'/'.$rnd;
            }
	}


public function SaveUserImage($file, $wi, $he){

        	$file_location = 'images/uploads/';
        

        	$dir =  absolute_path($file_location);
        	// Set the allowed file extensions
        	$fileTypes = array('jpg', 'jpeg', 'gif', 'png', 'bmp', 'txt'); // Allowed file extensions

			$tempFile   = $file['Filedata']['tmp_name'];
            $uploadDir  = $dir;
            $targetFile = $uploadDir . $file['Filedata']['name'];
            $fileParts = pathinfo($file['Filedata']['name']);
            if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
            	$this->ci->benchmark->mark('code_start');

          
                    $rnd = md5(time()).'.jpg';
                    move_uploaded_file($tempFile, $targetFile);
                    $img = new imagick($targetFile);
                    $img->setImageFormat('jpeg');
                    $d = $img->getImageGeometry();
                    $w = $d['width'];
                    if($w > $wi) 
                    $img->scaleImage($wi,0);
                	$d = $img->getImageGeometry();
                    $h = $d['height'];
                    if($h > $he) 
                    $img->scaleImage(0,$he);


                    $img->writeImage($dir.$rnd ); //output
                    $this->ci->benchmark->mark('code_end');
                    log_message('debug', 'TIME: RESIZE:'.$this->ci->benchmark->elapsed_time('code_start', 'code_end'));
                    $img->clear();
                    $img->destroy();
                    unlink($targetFile);




                    //$this->ci->cfiles->cf_container = 'cf_preview_image';
                    //$container_info = $this->ci->cfiles->container_info();
                    $url_container = 'http://96eb79f23583d5eba03b-3af84752bcb4e888a5f5d5c0deea4f72.r54.cf2.rackcdn.com';
                    $file = $dir.$rnd;
                    $this->ci->benchmark->mark('code_start');
                    //var_dump($this->ci->opencloud->list_containers()); 
                    $this->ci->opencloud->set_container('cf_user_wall');
					$this->ci->opencloud->add_object($rnd, file_get_contents($file), 'image/jpeg');
                    $this->ci->benchmark->mark('code_end');
                    unlink($file);
                    log_message('debug', 'TIME: WRITE:'.$this->ci->benchmark->elapsed_time('code_start', 'code_end'));
                    
                    $response = array(
                    	'url' => $url_container.'/'.$rnd,
                    	'image'=> $rnd
                    	);
                    return $response;
            }
	}


public function SaveUserAvatar($file, $wi, $he){

        	$file_location = 'images/uploads/';
        

        	$dir =  absolute_path($file_location);
        	// Set the allowed file extensions
        	$fileTypes = array('jpg', 'jpeg', 'gif', 'png', 'bmp', 'txt'); // Allowed file extensions

			$tempFile   = $file['Filedata']['tmp_name'];
            $uploadDir  = $dir;
            $targetFile = $uploadDir . $file['Filedata']['name'];
            $fileParts = pathinfo($file['Filedata']['name']);
            if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
            	$this->ci->benchmark->mark('code_start');

          
                    $rnd = md5(time()).'.jpg';
                    move_uploaded_file($tempFile, $targetFile);
                    $img = new imagick($targetFile);
                    $img->setImageFormat('jpeg');
                    $d = $img->getImageGeometry();
                    $w = $d['width'];
                    if($w > $wi) 
                    $img->scaleImage($wi,0);
                	$d = $img->getImageGeometry();
                    $h = $d['height'];
                    if($h > $he) 
                    $img->scaleImage(0,$he);


                    $img->writeImage($dir.$rnd ); //output
                    $this->ci->benchmark->mark('code_end');
                    log_message('debug', 'TIME: RESIZE:'.$this->ci->benchmark->elapsed_time('code_start', 'code_end'));
                    $img->clear();
                    $img->destroy();
                    unlink($targetFile);
                    //$this->ci->cfiles->cf_container = 'cf_preview_image';
                    //$container_info = $this->ci->cfiles->container_info();
                    $url_container = 'http://9ac8d72b6720ecc473ed-a223fb79f69743ae1a2104c258d978d3.r75.cf2.rackcdn.com';
                    $file = $dir.$rnd;
                    $this->ci->benchmark->mark('code_start');
                    //var_dump($this->ci->opencloud->list_containers()); 
                    $this->ci->opencloud->set_container('cf_user_image');
					$this->ci->opencloud->add_object($rnd, file_get_contents($file), 'image/jpeg');
                    $this->ci->benchmark->mark('code_end');
                    unlink($file);
                    log_message('debug', 'TIME: WRITE:'.$this->ci->benchmark->elapsed_time('code_start', 'code_end'));
                    
                    $response = array(
                    	'url' => $url_container.'/'.$rnd,
                    	'image'=> $rnd
                    	);
                    return $response;
            }
	}

public function migrateUserImage($dir, $file = null){
        $resizeImage = new Services_CF_ResizeImage;
        $image = $dir.$file;

        //ORIGINAL IMAGE
        $resizePostImage = $resizeImage->resizeImage($image, '160', '160', $file);
        $postImageName = $resizePostImage['name'];
        $postImageDir = $resizePostImage['path'];
        $uploadImg = file_get_contents($postImageDir.$postImageName);
        $mimeUpload = mime_content_type($postImageDir.$postImageName);
        $this->ci->benchmark->mark('code_start');
        $this->ci->opencloud->set_container('cf_user_image');
        $this->ci->opencloud->add_object($postImageName, $uploadImg, $mimeUpload );
        $this->ci->benchmark->mark('code_end');
        //log_message('debug', 'TIME: ORIGINAL:'.$this->ci->benchmark->elapsed_time('code_start', 'code_end'). 'SIZE:'.filesize($postImageDir.$postImageName));
        unlink($postImageDir.$postImageName);
        return array('name'=>$postImageName, 'saved'=>'true');

}

public function SaveSinglePost($dir, $file, $challenge = null){
		$resizeImage = new Services_CF_ResizeImage;
		$image = $dir.$file;

		//ORIGINAL IMAGE
		$resizePostImage = $resizeImage->resizeImage($image, '1024', '768', $file);
		$postImageName = $resizePostImage['name'];
		$postImageDir = $resizePostImage['path'];
		$uploadImg = file_get_contents($postImageDir.$postImageName);
		$mimeUpload = mime_content_type($postImageDir.$postImageName);
		$this->ci->benchmark->mark('code_start');
		$this->ci->opencloud->set_container('cf_original_image');
		$this->ci->opencloud->add_object($postImageName, $uploadImg, $mimeUpload );
		$this->ci->benchmark->mark('code_end');
        log_message('debug', 'TIME: ORIGINAL:'.$this->ci->benchmark->elapsed_time('code_start', 'code_end'). 'SIZE:'.filesize($postImageDir.$postImageName));
		unlink($postImageDir.$postImageName);

		//POST IMAGE
		$resizePostImage = $resizeImage->resizeImage($image, '439', '439', $file);
		$postImageName = $resizePostImage['name'];
		$postImageDir = $resizePostImage['path'];
		$this->ci->benchmark->mark('code_start');
		$uploadImg = file_get_contents($postImageDir.$postImageName);
		$mimeUpload = mime_content_type($postImageDir.$postImageName);
		$this->ci->opencloud->set_container('cf_web_wall');
		$this->ci->opencloud->add_object($postImageName, $uploadImg, $mimeUpload );
		$this->ci->benchmark->mark('code_end');
        log_message('debug', 'TIME: POST:'.$this->ci->benchmark->elapsed_time('code_start', 'code_end'). 'SIZE:'.filesize($postImageDir.$postImageName));
		unlink($postImageDir.$postImageName);

		//POST THUMBNAIL

		$resizePostImage = $resizeImage->resizeImage($image, '130', '130', $file);
		$postImageName = $resizePostImage['name'];
		$postImageDir = $resizePostImage['path'];
		$this->ci->benchmark->mark('code_start');
		$uploadImg = file_get_contents($postImageDir.$postImageName);
		$mimeUpload = mime_content_type($postImageDir.$postImageName);
		$this->ci->opencloud->set_container('cf_web_thumb');
		$this->ci->opencloud->add_object($postImageName, $uploadImg, $mimeUpload );
		$this->ci->benchmark->mark('code_end');
        log_message('debug', 'TIME: THUMB:'.$this->ci->benchmark->elapsed_time('code_start', 'code_end'). 'SIZE:'.filesize($postImageDir.$postImageName));	
		unlink($postImageDir.$postImageName);



		//MOBILE IMAGE
		$resizePostImage = $resizeImage->resizeImageMobile($image, '622', '476', $file);
		$postImageName = $resizePostImage['name'];
		$postImageDir = $resizePostImage['path'];
		$uploadImg = file_get_contents($postImageDir.$postImageName);
		$mimeUpload = mime_content_type($postImageDir.$postImageName);
		$this->ci->benchmark->mark('code_start');
		$this->ci->opencloud->set_container('cf_mobile_wall');
		$this->ci->opencloud->add_object($postImageName, $uploadImg, $mimeUpload );
		$this->ci->benchmark->mark('code_end');
        log_message('debug', 'TIME: MOBILE:'.$this->ci->benchmark->elapsed_time('code_start', 'code_end'). 'SIZE:'.filesize($postImageDir.$postImageName));
		unlink($postImageDir.$postImageName);

		//MOBILE THUMB
		$resizePostImage = $resizeImage->resizeImage($image, '98', '98', $file);
		$postImageName = $resizePostImage['name'];
		$postImageDir = $resizePostImage['path'];
		$uploadImg = file_get_contents($postImageDir.$postImageName);
		$mimeUpload = mime_content_type($postImageDir.$postImageName);
		$this->ci->benchmark->mark('code_start');
		$this->ci->opencloud->set_container('cf_mobile_thumb');
		$this->ci->opencloud->add_object($postImageName, $uploadImg, $mimeUpload);
		$this->ci->benchmark->mark('code_end');
        log_message('debug', 'TIME: MOBILETH:'.$this->ci->benchmark->elapsed_time('code_start', 'code_end'). 'SIZE:'.filesize($postImageDir.$postImageName));
		unlink($postImageDir.$postImageName);

		if ($challenge == 1){
			$this->saveChallengeImage($file);
		}
		return true;

	}


	function saveChallengeImage($image){
		$resizeImage = new Services_CF_ResizeImage;
		$cloud_file_name = $image;
		$file_location = 'images/uploads/challenges';
		$file_location = absolute_path($file_location);
		
        $this->ci->cfiles->cf_container = 'cf_original_image';
		$this->ci->cfiles->download_object($cloud_file_name, $cloud_file_name, $file_location);
		$newFilePath = $file_location.$image;
        //WEB CHALLENGE IMAGE
		$resizePostImage = $resizeImage->resizeImage($newFilePath, '439', '291', $image);
		$postImageName = $resizePostImage['name'];
		$postImageDir = $resizePostImage['path'];
		$this->ci->opencloud->set_container('cf_challenge_web');
		$this->ci->opencloud->add_object($postImageName, file_get_contents($postImageDir.$postImageName), 'image/jpeg');
		//unlink($postImageDir.$postImageName);
		//MOBILE CHALLENGE IMAGE
		$resizePostImage = $resizeImage->resizeImageMobile($newFilePath, '190', '146', $image);
		$postImageName = $resizePostImage['name'];
		$postImageDir = $resizePostImage['path'];
		$this->ci->opencloud->set_container('cf_challenge_mobile');
		$this->ci->opencloud->add_object($postImageName, file_get_contents($postImageDir.$postImageName), 'image/jpeg');
		//THUMB CHALLENGE IMAGE
		$resizePostImage = $resizeImage->resizeImage($newFilePath, '119', '73', $image);
		$postImageName = $resizePostImage['name'];
		$postImageDir = $resizePostImage['path'];
		$this->ci->opencloud->set_container('cf_challenge_thumb');
		$this->ci->opencloud->add_object($postImageName, file_get_contents($postImageDir.$postImageName), 'image/jpeg');

		unlink($postImageDir.$postImageName);
		unlink($file_location.$image);
		
		return true;
	}
}