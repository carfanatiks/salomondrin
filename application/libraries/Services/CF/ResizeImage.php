<?php

class Services_CF_ResizeImage
{

     
     public $testing = '1';
     public $container  = 'cf_thumbs';


     /**
      * The Mobile constructor
      */
     public function __construct() {
          $this->ci =& get_instance();

        $this->ci->load->helper('path');
        $this->ci->load->library('cf/cfiles');
       // $this->ci->cfiles->cf_container =  $this->container;

     }                 


     public function resizeImage($image, $wi, $hi, $filename){
          $file_location = 'images/uploads/main/';
          $dir =  absolute_path($file_location);
          $img = new imagick($image);
          $img->setImageFormat('jpeg');
          $d = $img->getImageGeometry();
          $w = $d['width'];
          if($w > $wi) 
          $img->scaleImage($wi,0);
          $d = $img->getImageGeometry();
          $h = $d['height'];
          if($h > $hi) 
          $img->scaleImage(0,$hi);
          $img->stripImage();
          $img->writeImage($dir.$filename ); //output
          $img->clear();
          $img->destroy();
          $image_info = array('name' => $filename, 'path' =>$dir);
          return $image_info;
     }
     public function resizeImageMobile($image, $wi, $hi, $filename){



        $file_location = 'images/uploads/main/';
        $dir =  absolute_path($file_location);
        $background = preg_match('/\.gif$|\.png$/', $image) == 1 ? 'None' : 'white';

        $img = new Imagick($image);
        $img->setImageFormat('jpeg');
        $d = $img->getImageGeometry();
        $w = $d['width'];
        if($w > $wi) 
        $img->scaleImage($wi,0);
        $d = $img->getImageGeometry();
        $h = $d['height'];
        if($h > $hi) 
        $img->scaleImage(0,$hi);

        $w = $img->getImageWidth();
        $h = $img->getImageHeight();

        $bkg = new Imagick();
        $bkg->newImage($wi, $hi, new ImagickPixel('white'));
        $bkg->setImageFormat('jpeg');
        $bkg->compositeImage($img, Imagick::COMPOSITE_DEFAULT,($wi-$w)/2,($hi-$h)/2 );


        /*$img->setImageBackgroundColor($background);
        
        $img->extentImage($wi,$hi,($wi-$w)/2,($hi-$h)/2);*/

          //$bkg->stripImage();
          $bkg->writeImage($dir.$filename ); //output
          $bkg->clear();
          $bkg->destroy();
          $img->clear();
          $img->destroy();
          $image_info = array('name' => $filename, 'path' =>$dir);
          return $image_info;
     }


}