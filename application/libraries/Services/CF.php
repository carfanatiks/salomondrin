<?php

function Services_CF_autoload($className) {

	if((substr($className, 0,11))!= 'Services_CF') {
		return false;
	}
	$file = str_replace('_', '/', $className);
  	$file = str_replace('Services', '', $file);
	return include_once dirname(__FILE__) . "$file.php";
}

    spl_autoload_register('Services_CF_autoload');


