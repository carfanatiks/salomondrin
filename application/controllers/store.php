<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('blog_model');
		$this->load->model('products_model');
	}

	public function index()
	{
		

		if(!$this->ion_auth->logged_in()) // block un-authorized access
	    {
	        //redirect('auth');
	    }

	    $user = $this->ion_auth->user()->row();
	    $products = $this->products_model->get_products();
	    $data['user'] = $user;
	    $data['products'] = $products;
		$data['title'] = 'Products - '.$this->config->item('site_title', 'ion_auth');		
		// set current menu highlight
		
		// render view
		$this->load->view('blog/store',$data);
	}
	
	public function detail($id = null)
	{
		

		if(!$this->ion_auth->logged_in()) // block un-authorized access
	    {
	        //redirect('auth');
	    }
	    if ($id == null || $id < 1){
	    	//redirect('store');
	    }

	    $user = $this->ion_auth->user()->row();
	    $product = $this->products_model->get_product($id);
	    $data['user'] = $user;
	    $data['product'] = $product;
	    $disabled = '';
        if ($product['pp_value'] > $user->points){
              $disabled = 'disabled="disabled"';
        }
        $data['disabled'] =  $disabled;

		$data['title'] = $product['productName'] .' - '.$this->config->item('site_title', 'ion_auth');		
		// set current menu highlight
		
		// render view
		$this->load->view('blog/detail',$data);
	}
	


}