<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Images extends CI_Controller {
 
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Purchases_model','purchases');
        $this->load->model('Images_model');
        $this->load->helper('path');
        
        $this->load->library('opencloud');
    }
 


    public function save(){


        $images = $this->Images_model->getImages();

        $file_location = 'uploads/';
        $dir =  absolute_path($file_location);
        $dir2 =  absolute_path('uploads2/');
        $fileTypes = array('jpg', 'jpeg', 'gif', 'png', 'bmp');
        $i = 0;
        foreach ($images as $imageData){
            $i++;
            
            $imageName = $imageData['image_up'];
            $image = file_get_contents($dir.$imageName);
           
            $imageId = $imageData['id'];
            $tempFile   = $dir.$imageName;
            $targetFile = $dir2.$imageName;
            $singleName =  preg_replace('/\\.[^.\\s]{3,4}$/', '', $imageName);
            $singleName = strtolower($singleName);
            $newName = $singleName.'.jpg';
            copy($tempFile, $targetFile);
            $img = new imagick($targetFile);
            $img->setImageFormat('jpeg');
            $d = $img->getImageGeometry();
            $w = $d['width'];
            if($w > 1280) 
            $img->scaleImage(1280,0);
            $d = $img->getImageGeometry();
            $h = $d['height'];
            if($h > 720) 
            $img->scaleImage(0,720);
            $img->writeImage($dir2.$newName ); //output
            $img->clear();
            $img->destroy();

            $url_container = 'http://86839a4a03d6d7304c9d-c1a65037a58eb95f6995e24c723ab9d6.r58.cf1.rackcdn.com';
            $file = $dir2.$newName;
            
            
            $this->opencloud->set_container('images');
            $this->opencloud->add_object($newName, file_get_contents($file), 'image/jpeg');
           

            echo "nombre archivo: $file : tipo archivo: " . ($file) . "\n";
            $newUrl =  $url_container.'/'.$newName;

            $this->Images_model->edit_upload($imageId, $newUrl);
            $bodyEntries = $this->Images_model->getImageBody($imageName);
            foreach ($bodyEntries as $body){
                $bodytag = $this->str_replace_deep('img src="/uploads/'.$imageName.'"', 'img src="'.$newUrl.'"', $body['entry_body']);
                echo $bodytag;
                $this->Images_model->edit_entry($body['entry_id'], $bodytag);
            }

            $entryThumbs = $this->Images_model->getImageThumb($imageName);

            foreach ($entryThumbs as $thumbs){

                $entryId = $thumbs["entry_id"];
                $this->Images_model->edit_entry_thumb($entryId, $newUrl);
               

            }

        }


    }


    function str_replace_deep($search, $replace, $subject) 
{ 
    if (is_array($subject)) 
    { 
        foreach($subject as &$oneSubject) 
            $oneSubject = str_replace_deep($search, $replace, $oneSubject); 
        unset($oneSubject); 
        return $subject; 
    } else { 
        return str_replace($search, $replace, $subject); 
    } 
}
}