<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Purchases extends CI_Controller {
 
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Purchases_model','purchases');
    }
 
    public function index()
    {
        $this->load->helper('url');
        $this->load->view('admin/purchases');
    }
 
    public function ajax_list()
    {
        $list = $this->purchases->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $purchases) {
            $no++;
            $row = array();
            $row[] = $purchases->name;
            $row[] = $purchases->lastname;
            $row[] = $purchases->email;
            $row[] = $purchases->address;
            $row[] = $purchases->address2;
            $row[] = $purchases->city;
            $row[] = $purchases->state;
            $row[] = $purchases->zipCode;
            $row[] = $purchases->phone;
            $row[] = $purchases->mobile;
            $row[] = $purchases->status;
            $row[] = $purchases->trackingCode;
 
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_p('."'".$purchases->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
         
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->purchases->count_all(),
                        "recordsFiltered" => $this->purchases->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
 
    public function ajax_edit($id)
    {
        $data = $this->purchases->get_by_id($id);
        //$data->dob = ($data->dob == '0000-00-00') ? '' : $data->dob; // if 0000-00-00 set tu empty for datepicker compatibility
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
        $this->_validate();
        $data = array(
                'name' => $this->input->post('name'),
                'lastname' => $this->input->post('lastname'),
                'email' => $this->input->post('email'),
                'address' => $this->input->post('address'),
                'address2' => $this->input->post('address2'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'zipCode' => $this->input->post('zipCode'),
                'phone' => $this->input->post('phone'),
                'mobile' => $this->input->post('mobile'),
                'idStatus' => $this->input->post('idStatus'),
            );
        $insert = $this->purchases->save($data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_update()
    {
        $this->_validate();
        $data = array(
                'name' => $this->input->post('name'),
                'lastname' => $this->input->post('lastname'),
                'email' => $this->input->post('email'),
                'address' => $this->input->post('address'),
                'address2' => $this->input->post('address2'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'zipCode' => $this->input->post('zipCode'),
                'phone' => $this->input->post('phone'),
                'mobile' => $this->input->post('mobile'),
                'idStatus' => $this->input->post('idStatus'),
            );
        $this->purchases->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_delete($id)
    {
        $this->purchases->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
 
 
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('name') == '')
        {
            $data['inputerror'][] = 'name';
            $data['error_string'][] = 'First name is required';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('lastname') == '')
        {
            $data['inputerror'][] = 'lastname';
            $data['error_string'][] = 'Last name is required';
            $data['status'] = FALSE;
        }
 

 

 
        if($this->input->post('address') == '')
        {
            $data['inputerror'][] = 'address';
            $data['error_string'][] = 'Addess is required';
            $data['status'] = FALSE;
        }
 
        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
 
}