<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Points extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('blog_model');
	}

	public function index()
	{
		// set page title
		$data['title'] = ''.$this->config->item('site_title', 'ion_auth');		
		// set current menu highlight
		$data['current'] = 'Products';		
		// get all post
		$data['posts'] = $this->blog_model->get_posts();		
		// get all categories for sidebar menu
		$data['categories'] = $this->blog_model->get_categories();		
		// render view
		$this->load->view('blog/index',$data);
	}
	
	public function add_products()
	{
	    if( ! $this->ion_auth->logged_in() && ! $this->ion_auth->is_admin() ) // block un-authorized access
	    {
	        show_404();
	    }
	    else
	    {
	        $data['title'] = 'Add Products - '.$this->config->item('site_title', 'ion_auth');
	        $data['categories'] = $this->blog_model->get_categories();

	        $this->load->helper('form');
	        $this->load->library(array('form_validation'));
	 		$file_element_name = 'image_up';
	        //set validation rules
	        //$this->form_validation->set_rules('image_up', 'Image', 'required|xss_clean');
		 	
	        if (empty($_FILES['image_up']['tmp_name']))
	        {
	            //if not valid
	            
	            $this->load->view('admin/add_product',$data);
	        }
	        else
	        {

	            //if valid
	            
	            
	            if (!empty($_FILES['image_up']['tmp_name'])) {

	            
		            $config['upload_path'] = './uploads/';
		            $config['allowed_types'] = '*';
		            $config['max_size'] = 0;
		            $this->load->library('upload', $config);
		            
		            //$this->upload->initialize($config);
	            
		            if (!$this->upload->do_upload($file_element_name)) {
		            	
						// $this->session->set_flashdata('message', 'Error uploading image');
		            } else {
		                $dataUploaded = $this->upload->data();

		                $file_name = $dataUploaded['file_name'];
		                $file_path = '/uploads/'.$file_name;

		                $name = $this->input->post('name');
		                $description = $this->input->post('description');
		                $pp_value = $this->input->post('pp_value');

		                $dataProd = array(
		                	'name' => $name,
		                	'description' => $description,
		                	'pp_value' => $pp_value
		                	);

		                
		                 $idProduct = $this->blog_model->add_products($dataProd);
		                 
		                 $productImage = array(
			            	'idProduct' => $idProduct,
			            	'name' => $file_name,
			            	'url' => $file_path
		            	);


			            $this->blog_model->add_product_image($productImage);
			            $this->session->set_flashdata('message', '1 new product added!');
		                
		            }
		            
		           

		            
		            redirect('products');
	        	}
	    	}
	   	}
	}

	public function fillproducts(){
        $this->blog_model->fillproducts();
    }

/* DELETE IMAGES FROM LIBRARY */
    public function delete_prod($id = '') {
	  $this->load->model('blog_model');
	  $where = array('idProduct' => $id); 
	  $this->blog_model->delete_prod('products',$where);
	  echo'<div class="alert alert-success">Product '.$id.' deleted Successfully</div>';
            exit;
	}



	public function add_points()
	{
	    if( ! $this->ion_auth->logged_in() && ! $this->ion_auth->is_admin() ) // block un-authorized access
	    {
	        show_404();
	    }
	    else
	    {

    		$data['title'] = 'Add Cupons - '.$this->config->item('site_title', 'ion_auth');
	        $data['categories'] = $this->blog_model->get_categories();

	        $this->load->helper('form');
	        $this->load->library(array('form_validation'));

	    	if (empty($this->input->post('cupon')) || empty($this->input->post('pp_value'))){
	    		$this->load->view('admin/add_points',$data);
	    	} else {

	            $name = $this->input->post('cupon');

	            $pp_value = $this->input->post('pp_value');

	            $validto = $this->input->post('validto');
	            $uses = $this->input->post('uses');


	            $cuponData = array(
	            	'cupon' => $name,
	            	'value' => $pp_value,
	            	'validTo' => $validto,
	            	'uses' => $uses, 
	            	'global' => $uses
	        	);

	            
	            $idCupon= $this->blog_model->add_cupons($cuponData);	          
	            $this->session->set_flashdata('added', '1 cupon!');
				redirect('cupons');
			}
	        	
	    	
	   	}
	}

	public function fillcupons(){
        $this->blog_model->fillcupons();
    }


}