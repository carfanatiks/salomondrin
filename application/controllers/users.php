<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('blog_model');
	}

	public function index($id = null)
	{
		if ($id == null)
			redirect ('blog');


		$user = $this->ion_auth->user()->row();
		$data['user'] = $user;
		
		// set page title
		$data['title'] = 'Profile '.$this->config->item('site_title', 'ion_auth');		
		// set current menu highlight
		if (strlen($this->input->post('pennis_points')) < 1) {
		
			$this->load->view('blog/users',$data);
		} else {
			$cupon = $this->input->post('pennis_points');
			
			$cuponData = $this->blog_model->checkCupon($cupon);
			if ($cuponData != false){
				$checkCuponUsed = $this->blog_model->notUsed($cuponData['idpoint'], $user->id);
				if ($checkCuponUsed){
					$currentPoints = $user->points + $cuponData['value'];
					$dataInsert = array(
						'id_user' => $user->id,
						'id_points' => $cuponData['idpoint'],
						'cupon' => $cupon,
						'value' =>$cuponData['value']
						);
					$dataUpdate = array(
						'points' => $currentPoints
						);
					$history_points = $user->history_points + $cuponData['value'];
					$totalUses = $cuponData['uses'] - 1;
					$this->blog_model->insertPennis($dataInsert, $history_points, $totalUses);
					$this->blog_model->updatePennis($dataUpdate, $user->id);
					$this->session->set_flashdata('message', 'Reedem Successful');


				} else {

					$this->session->set_flashdata('message', 'CUPON ALREADY USED');
				}



			} else {
				// INVALID CUPON

				$cuponGralUses = $this->blog_model->checkCuponGralUses($cupon);
					if ($cuponGralUses['idpoint'] > 0 && $cuponGralUses['uses'] == 0){
						$this->session->set_flashdata('message', 'Cupon reached usage limit');
					} else {
						$this->session->set_flashdata('message', 'INVALID CUPON');
					}
			}

			redirect('/users/'.$user->id);
		}
	}

}
	