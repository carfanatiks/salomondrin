<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends MX_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('blog_model');
		$this->load->helper('path');
        
        $this->load->library('opencloud');
	}

	public function index($id = null)
	{


		if ($id == null){
			$start= 0;
		} else {
			$start = $id;
		}
		$page = 15;
		// set page title
		$data['title'] = ''.$this->config->item('site_title', 'ion_auth');		
		// set current menu highlight
		$data['current'] = 'HOME';		
		// get all post
		$data['posts'] = $this->blog_model->get_paginated_posts($start, $page);	
		
		// get all categories for sidebar menu
		//$data['categories'] = $this->blog_model->get_categories();		
		// render view
		$this->load->view('blog/index',$data);
	}

	public function paginated($id = null)
	{


		if ($id == null){
			$start= 0;
		} else {
			$start = $id;
		}
		$page = 15;
		// set page title
		$data['title'] = ''.$this->config->item('site_title', 'ion_auth');		
		// set current menu highlight
		$data['current'] = 'HOME';		
		// get all post
		$data['posts'] = $this->blog_model->get_paginated_posts($start, $page);	
		
		// get all categories for sidebar menu
		//$data['categories'] = $this->blog_model->get_categories();		
		// render view
		$this->load->view('blog/paginatedblog',$data);
	}
	
	public function about()
	{
		$data['title'] = 'About - '.$this->config->item('site_title', 'ion_auth');
		$data['current'] = 'ABOUT';
		$this->load->view('blog/about',$data);
	}
	public function cupgang()
	{
		$data['title'] = 'Cupgang - '.$this->config->item('site_title', 'ion_auth');
		$data['current'] = 'CUPGANG';
		$this->load->view('blog/cupgang',$data);
	}
	public function events()
	{
		$data['title'] = 'Events - '.$this->config->item('site_title', 'ion_auth');
		$data['current'] = 'EVENTS';
		// get all events
		$data['events'] = $this->blog_model->get_events();
		$data['past_events'] = $this->blog_model->get_past_events();

		$this->load->view('blog/events',$data);
	}
	
	public function post($id) // get a post based on id
	{


		if (!is_int($id)){
			 $postData = $this->blog_model->get_id_by_sef($id);
			 $id = $postData['entry_id'];
		}

		$data['query'] 			= $this->blog_model->get_post($id);
		$data['comments'] 		= $this->blog_model->get_post_comment($id); // get comments related to the post
		$data['post_id'] 		= $id;
		$data['categories'] = $this->blog_model->get_categories();
		
		if( $this->ion_auth->logged_in() )
			$data['user'] = $this->ion_auth->user()->row(); // get current user login details
		
		$this->load->helper('form');
		$this->load->library(array('form_validation'));
		
		//set validation rules
		$this->form_validation->set_rules('commentor', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('comment', 'Comment', 'required');
		
		if($this->blog_model->get_post($id))
		{
			foreach($this->blog_model->get_post($id) as $row)
			{
				//set page title
				$data['title'] = $row->entry_name.' - '.$this->config->item('site_title', 'ion_auth');
			}
			
			if ($this->form_validation->run() == FALSE)
			{
				//if not valid
				$this->load->view('blog/post',$data);
			}
			else
			{
				//if valid
				$name = $this->input->post('commentor');
				$email = strtolower($this->input->post('email'));
				$comment = $this->input->post('comment');
				$post_id = $this->input->post('post_id');
				
				if( $this->input->post('user_id') )
					$user_id = $this->input->post('user_id');
				else
					$user_id = 0;
				
				//$this->blog_model->add_new_comment($post_id, $name, $email, $comment, $user_id);
				$this->session->set_flashdata('message', '1 new comment added!');
				redirect('post/'.$id);
			}
		}
		else
			show_404();
	}
public function all_entries()
	{
		$data['title'] = 'All Entries - '.$this->config->item('site_title', 'ion_auth');
		$data['current'] = 'All Entries';
		// get all events
		$data['all_entries'] = $this->blog_model->get_events();
		$data['entry'] = $this->blog_model->get_past_events();

		$this->load->view('admin/all_entries',$data);
	}

/* CREATE NEW ENTRIES */
	public function add_new_entry()
	{

	    if( ! $this->ion_auth->logged_in() && ! $this->ion_auth->is_admin() ) // block un-authorized access
	    {
	        show_404();
	    }
	    else
	    {

	        $data['title'] = 'Add new entry - '.$this->config->item('site_title', 'ion_auth');
	        $data['categories'] = $this->blog_model->get_categories();

	        $this->load->helper('form');
	        $this->load->library(array('form_validation'));
	 
	        //set validation rules
	        $this->form_validation->set_rules('entry_name', 'Title', 'required|max_length[200]|xss_clean');
	        $this->form_validation->set_rules('friend_url', 'Friend URL', 'required|max_length[200]|xss_clean');
	        $this->form_validation->set_rules('entry_thumb', 'Thumb', 'required|xss_clean');
	        $this->form_validation->set_rules('entry_body', 'Body', 'required|xss_clean');
	        $this->form_validation->set_rules('entry_category', 'Category', '');	
	 
	        if ($this->form_validation->run() == FALSE)
	        {
	            //if not valid
	            $this->load->view('admin/add_new_entry',$data);
	        }
	        else
	        {
	            //if valid
	            $user = $this->ion_auth->user()->row();
	            $title = $this->input->post('entry_name');
	            $thumb = $this->input->post('entry_thumb');
	            $body = $this->input->post('entry_body');
	            $sef = $this->input->post('friend_url');
	            $categories = $this->input->post('entry_category');

	            $this->blog_model->add_new_entry($user->id,$title,$body,$categories,$thumb, $sef);
	            $this->session->set_flashdata('message', '1 new post added!');
	            redirect('add-new-entry');
	        }
	    }
	}
/* EDIT ENTRIES */
	public function edit_entry(){

	        $this->load->helper('form');
	        $this->load->library(array('form_validation'));
	        $this->form_validation->set_rules('entry_name', 'Title', 'required|max_length[200]|xss_clean');
	        $this->form_validation->set_rules('friend_url', 'Friend URL', 'required|max_length[200]|xss_clean');
	        $this->form_validation->set_rules('entry_thumb', 'Thumb', 'required|xss_clean');
	        $this->form_validation->set_rules('entry_body', 'Body', 'required|xss_clean');
	        $this->form_validation->set_rules('entry_category', 'Category', '');	
	 
            $id =  $this->uri->segment(3);
            $this->db->where('entry_id',$id);
            $data['query'] = $this->db->get('entry');
            $data['entry_id'] = $id;
            

             if( ! $this->ion_auth->logged_in() && ! $this->ion_auth->is_admin() ) // block un-authorized access
	    	{
	        	show_404();
	    	}
	   		else
	    	{

		    	if ($this->form_validation->run() == FALSE)
		        {
		            //if not valid
		            $this->load->view('admin/edit_entry', $data);
		        }
		        else
		        {
	//if valid
		            $user = $this->ion_auth->user()->row();
		            $title = $this->input->post('entry_name');
		            $thumb = $this->input->post('entry_thumb');
		            $body = $this->input->post('entry_body');
					$sef=$this->input->post('friend_url');


		            
		            $categories = $this->input->post('entry_category');

		            $this->blog_model->edit_entry($id, $user->id,$title,$body,$categories,$thumb, $sef);
		            $this->session->set_flashdata('message', 'Post Edited Successfully!');
		            redirect('blog/edit_entry/'.$id);

		        }

		    }

   	}
 /* LOADING LIST OF ALL ENTRIES */
	public function fillentries(){
        $this->blog_model->fillentries();
    }
/* DELETE ENTRY */
    public function delete_entry($id = '') {
	  $this->load->model('blog_model');
	  $where = array('entry_id' => $id); 
	  $this->blog_model->delete_entry('entry',$where);
	   echo'<div class="alert alert-success">One record deleted Successfully</div>';
            exit;
	}


/* UPLOAD IMAGES */
	public function add_photos()
	{
	    if( ! $this->ion_auth->logged_in() && ! $this->ion_auth->is_admin() ) // block un-authorized access
	    {
	        show_404();
	    }
	    else
	    {
	        $data['title'] = 'Images Library - '.$this->config->item('site_title', 'ion_auth');
	        $data['categories'] = $this->blog_model->get_categories();

	        $this->load->helper('form');
	        $this->load->library(array('form_validation'));
	 		$file_element_name = 'image_up';
	        //set validation rules
	        //$this->form_validation->set_rules('image_up', 'Image', 'required|xss_clean');
		 	
	        if (empty($_FILES['image_up']['tmp_name']))
	        {
	            //if not valid
	            
	            $this->load->view('admin/add_photos',$data);
	        }
	        else
	        {

	            //if valid
	            
	            if (!empty($_FILES['image_up']['tmp_name'])) {

	            
	            $config['upload_path'] = './uploads/';
	            $config['allowed_types'] = '*';
	            $config['max_size'] = 0;
	            $this->load->library('upload', $config);
	            
	            //$this->upload->initialize($config);
	            
	            if (!$this->upload->do_upload($file_element_name)) {
	            	
					// $this->session->set_flashdata('message', 'Error uploading image');
	            } else {
	                $dataUploaded = $this->upload->data();

	                $file_name = $dataUploaded['file_name'];
	                $file_path = $config['upload_path'].$file_name;

	                $file = array();
	                
	            }
	            }
	            

		   	$file_location = 'uploads/';
	        $dir =  absolute_path($file_location);
	        $dir2 =  absolute_path('uploads2/');
	        $imageName = $file_name;
            $image = file_get_contents($dir.$imageName);
           
            $imageId = $imageData['id'];
            $tempFile   = $dir.$imageName;
            $targetFile = $dir2.$imageName;
            $singleName =  preg_replace('/\\.[^.\\s]{3,4}$/', '', $imageName);
            $singleName = strtolower($singleName);
            $newName = $singleName.'.jpg';
            copy($tempFile, $targetFile);
            $img = new imagick($targetFile);
            $img->setImageFormat('jpeg');
            $d = $img->getImageGeometry();
            $w = $d['width'];
            if($w > 1280) 
            $img->scaleImage(1280,0);
            $d = $img->getImageGeometry();
            $h = $d['height'];
            if($h > 720) 
            $img->scaleImage(0,720);
            $img->writeImage($dir2.$newName ); //output
            $img->clear();
            $img->destroy();

            $url_container = 'http://86839a4a03d6d7304c9d-c1a65037a58eb95f6995e24c723ab9d6.r58.cf1.rackcdn.com';
            $file = $dir2.$newName;
            
            
            $this->opencloud->set_container('images');
            $this->opencloud->add_object($newName, file_get_contents($file), 'image/jpeg');
           

           
            $newUrl =  $url_container.'/'.$newName;

	            $this->blog_model->add_photos($newUrl);
	            $this->session->set_flashdata('message', '1 new image added!');
	            redirect('add-photos');
	        }
	    }
	}

/* LOADING IMAGES ON LIBRARY */
	public function fillimages(){
        $this->blog_model->fillimages();
    }
/* DELETE IMAGES FROM LIBRARY */
    public function delete_img($id = '') {
	  $this->load->model('blog_model');
	  $where = array('id_up' => $id); 
	  $this->blog_model->delete_img('upload',$where);
	  echo'<div class="alert alert-success">One image deleted Successfully</div>';
            exit;
	}


/* ADD NEW EVENTS IN A GALLERY */
	public function add_new_event()
	{

	    if( ! $this->ion_auth->logged_in() && ! $this->ion_auth->is_admin() ) // block un-authorized access
	    {
	        show_404();
	    }
	    else
	    {

	        $data['title'] = 'Add new event - '.$this->config->item('site_title', 'ion_auth');
	        $data['categories'] = $this->blog_model->get_categories();

	        $this->load->helper('form');
	        $this->load->library(array('form_validation'));
	 
	        //set validation rules
	        $this->form_validation->set_rules('event_name', 'Ename', 'required|max_length[200]|xss_clean');
	        $this->form_validation->set_rules('event_date', 'Edate', 'required|xss_clean');
	        $this->form_validation->set_rules('event_place', 'Eplace', 'required|xss_clean');
	        $this->form_validation->set_rules('event_image', 'Eimage', 'required|xss_clean');	        
	 
	        if ($this->form_validation->run() == FALSE)
	        {
	            //if not valid
	            $this->load->view('admin/add_new_event',$data);
	        }
	        else
	        {
	            //if valid
	            $ename = $this->input->post('event_name');
	            $edate = $this->input->post('event_date');
	            $eplace = $this->input->post('event_place');
	            $eimage = $this->input->post('event_image');
	            

	            $this->blog_model->add_new_event($ename,$edate,$eplace,$eimage);
	            $this->session->set_flashdata('message', '1 new event added!');
	            redirect('add-new-event');
	        }
	    }
	}
/* LOADING PAST EVENTS TABLE IN ADMIN */
	public function fillpevents(){
        $this->blog_model->fillpevents();
    }
	
	public function add_past_event(){

	    if( ! $this->ion_auth->logged_in() && ! $this->ion_auth->is_admin() ) // block un-authorized access
	    {
	        show_404();
	    }
	    else
	    {

	        $data['title'] = 'Add past event - '.$this->config->item('site_title', 'ion_auth');
	        $data['categories'] = $this->blog_model->get_categories();

	        $this->load->helper('form');
	        $this->load->library(array('form_validation'));
	 
	        //set validation rules
	        $this->form_validation->set_rules('past_event_name', 'Event Name', 'required|max_length[200]|xss_clean');
	        $this->form_validation->set_rules('past_event_link', 'Link to a review', 'required|xss_clean');	
	
	 
	        if ($this->form_validation->run() == FALSE)
	        {
	            //if not valid
	            $this->load->view('admin/add_past_event',$data);
	        }
	        else
	        {
	            //if valid
	            $pastename = $this->input->post('past_event_name');
	            $pastelink = $this->input->post('past_event_link');
	            
	            $this->blog_model->add_past_event($pastename,$pastelink);
	            $this->session->set_flashdata('message', '1 new past event added!');
	            
	            redirect('add-past-event');
	        }
	    }
	}
	public function edit(){
        $id =  $this->uri->segment(3);
        $this->db->where('past_event_id',$id);
        $data['query'] = $this->db->get('past_event');
        $data['past_event_id'] = $id;
        $this->load->view('admin/edit', $data);
    }
	 public function update(){
        $res['error']="";
        $res['success']="";
        $this->form_validation->set_rules('past_event_name', 'Event Name', 'required');
        $this->form_validation->set_rules('past_event_link', 'Link to review', 'required');
        if ($this->form_validation->run() == FALSE){
        $res['error']='<div class="alert alert-danger">'.validation_errors().'</div>';    
        }           
	    else{
	        $data = array(
	        	'past_event_name'=>  $this->input->post('past_event_name'),
	        	'past_event_link'=>$this->input->post('past_event_link')
	        );
	        $this->db->where('past_event_id', $this->input->post('hidden'));
	        $this->db->update('past_event', $data);
	        $data['success'] = '<div class="alert alert-success">One record inserted Successfully</div>';
	    }
		    header('Content-Type: application/json');
		    echo json_encode($res);
		    exit;
    }

    public function delete(){
        $id =  $this->input->POST('past_event_id');
        $this->db->where('past_event_id', $id);
        $this->db->delete('past_event');
        echo'<div class="alert alert-success">One record deleted Successfully</div>';
        exit;
    }
        
	 
	public function add_new_category()
	{
	    $data['title'] = 'Add new category - '.$this->config->item('site_title', 'ion_auth');
	 
	    if( ! $this->ion_auth->logged_in() && ! $this->ion_auth->is_admin() ) // block un-authorized access
	    {
	        show_404();
	    }
	    else
	    {
	        $this->load->helper('form');
	        $this->load->library(array('form_validation'));
	 
	        // set validation rules
	        $this->form_validation->set_rules('category_name', 'Name', 'required|max_length[200]|xss_clean');
	        $this->form_validation->set_rules('category_slug', 'Slug', 'max_length[200]|xss_clean');
	 
	        if ($this->form_validation->run() == FALSE)
	        {
	            //if not valid
	            $this->load->view('admin/add_new_category',$data);
	        }
	        else
	        {
	            //if valid
	            $name = $this->input->post('category_name');
	 
	            if( $this->input->post('category_slug') != '' )
	                $slug = $this->input->post('category_slug');
	            else
	                $slug = strtolower(preg_replace('/[^A-Za-z0-9_-]+/', '-', $name));
	 
	            $this->blog_model->add_new_category($name,$slug);
	            $this->session->set_flashdata('message', '1 new category added!');
	            redirect('add-new-category');
	        }
	    }
	}
	
	public function category($slug = FALSE)
	{
		$data['title'] = 'Category - '.$this->config->item('site_title', 'ion_auth');
		$data['categories'] = $this->blog_model->get_categories();
		
		if( $slug == FALSE )
			show_404();
		else
		{
			$data['category'] = $this->blog_model->get_category(NULL,$slug); // get category details
			$data['query'] = $this->blog_model->get_category_post($slug); // get post in the category
		}
		
		$this->load->view('blog/category',$data);
	}

	
}

/* End of file blog.php */
/* Location: ./application/controllers/blog.php */