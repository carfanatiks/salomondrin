<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Redeem extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('blog_model');
		$this->load->model('products_model');
	}


	
	public function product($id = null)
	{
		

		if(!$this->ion_auth->logged_in()) // block un-authorized access
	    {
	        redirect('auth');
	    }
	    if ($id == null || $id < 1){
	    	redirect('store');
	    }

	    $user = $this->ion_auth->user()->row();
	    $product = $this->products_model->get_product($id);
	    $data['user'] = $user;
	    $data['product'] = $product;
	    $disabled = '';
        if ($product['pp_value'] > $user->points){
        	redirect('store');
              $disabled = 'disabled="disabled"';
        }
        $data['disabled'] =  $disabled;

		$data['title'] = $product['productName'] .' - '.$this->config->item('site_title', 'ion_auth');		
		// set current menu highlight
		
		// render view
		$this->load->view('blog/redeem',$data);
	}
	

	function success($id = null){


		if(!$this->ion_auth->logged_in()) // block un-authorized access
	    {
	        redirect('auth');
	    }
	    if ($id == null || $id < 1){
	    	redirect('store');
	    }

	    $user = $this->ion_auth->user()->row();
	    $product = $this->products_model->get_product($id);
	    $data['user'] = $user;
	    $data['product'] = $product;
	    $disabled = '';
        if ($product['pp_value'] > $user->points){
        	// NOT ENOUGH POINTS
              redirect('store');
              exit;
        } else {
        	$actualPoints = $user->points - $product['pp_value'];
        	$dataUpdatePoints = array(
						'points' => $actualPoints
						);
        }
        $dataInsert = array(
        	'idProduct' => $product['idProduct'],
        	'idUser' => $user->id,
        	'address' => $this->input->post('address'),
        	'address2' => $this->input->post('address2'),
        	'city' => $this->input->post('city'),
        	'state' => $this->input->post('state'),
        	'zipCode' => $this->input->post('zipCode'),
        	'phone' => $this->input->post('phone'),
        	'mobile' => $this->input->post('mobile'),
        	'name' => $this->input->post('name'),
        	'lastname' => $this->input->post('lastname'),
        	'email' => $this->input->post('email')


        	);

        $this->products_model->purchase($dataInsert);
        $this->blog_model->updatePennis($dataUpdatePoints, $user->id);
        $data['post_fields'] = $dataInsert;

       // Load additional models and libraries.
        $this->load->library('email');
        $this->load->library('encrypt');
        
        // Set up the email.

       
		$data['title'] = $product['productName'] .' - '.$this->config->item('site_title', 'ion_auth');	

        $body =  $this->load->view('mail/redeem',$data, TRUE);
        //$this->load->view('mail/mail_signup',$data);
        $this->email->from('contact@techrojo.com');
      	//$this->email->to('coffee@ipcoffees.com');
        //$this->email->to('asalomondrin@gmail.com');
        $this->email->to($this->input->post('email')); 
        $this->email->cc('marco@techrojo.com'); 
		//$this->email->bcc('marco@techrojo.com'); 
        $this->email->subject('Congratulations for your redemeed');
        $this->email->message($body);
		$this->email->send();


		 $body =  $this->load->view('mail/mailsend',$data, TRUE);
        //$this->load->view('mail/mail_signup',$data);
        $this->email->from('contact@techrojo.com');
      	//$this->email->to('coffee@ipcoffees.com');
        $this->email->to('info@salomondrin.com');
        //$this->email->to('marco@techrojo.com'); 
        //$this->email->cc('rodrigo@techrojo.com'); 
		//$this->email->bcc('marco@techrojo.com'); 
        $this->email->subject('New Redeem from '.$post_fields['name'].' '.$post_fields['lastname']);
        $this->email->message($body);
		$this->email->send();

		$this->load->view('mail/redeem',$data);

		


		
	}


}