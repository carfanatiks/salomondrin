<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| OpenCloud Username
|--------------------------------------------------------------------------
*/
$config['opencloud_username']	= "bluethy";

/*
|--------------------------------------------------------------------------
| OpenCloud API Key
|--------------------------------------------------------------------------
*/
$config['opencloud_key'] = "3caa3296eff94d6daf2e9d02bb188c08";

/*
|--------------------------------------------------------------------------
| OpenCloud Endpoint
|--------------------------------------------------------------------------
*/
$config['opencloud_endpoint'] = "https://identity.api.rackspacecloud.com/v2.0/";

/*
|--------------------------------------------------------------------------
| Region Identifier (e.g., "DFW" or "LON")
|--------------------------------------------------------------------------
*/
$config['opencloud_region'] = "DFW";


/* End of file opencloud.php */
/* Location: ./application/config/opencloud.php */