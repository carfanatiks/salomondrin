<?php $this->load->view('blog/header_admin');?>
	
	<div class="col-md-3">
		<!-- column-two -->
		<?php $this->load->view('blog/menu_sidebar');?>	
	</div>
	<div class="col-md-8">
		<h2>Add a Past Event to the list</h2>
		<hr>
		<div class="well">
			<?php echo form_open('add-past-event', 'id="addevent"');?>
			
    			<?php if(validation_errors()){echo validation_errors('<div class="alert alert-danger">','</p>'.'</div>');} ?>
    		    <?php if($this->session->flashdata('message')){echo '<div class="alert alert-success">'.$this->session->flashdata('message').'</div>';}?>	
    		    <div id="response"></div>
    		    <div class="form-group">
    				<label>Event Name:</label><br>
    				<input class="form-control" type="text" name="past_event_name" size="30" />
    			</div>
    			<div class="form-group">
    				<label >Link to a review (website/post):</label><br>
    				<input class="form-control" type="text" name="past_event_link" size="30" />
    			</div>	

    			<input class="btn btn-success" type="submit" value="Submit"/>
			<!--input class="button" type="reset" value="Reset"/-->	
		
			</form>
		</div>
		<br>
		<h2>Past Events List</h2>
		<hr>
		<table class="table">
            <thead>
            	<tr><th>Event Name</th><th>Review Link</th><th>Actions</th></tr>
            </thead>
            <tbody id="fillpevents">
            
            </tbody>
            <tfoot></tfoot>
        </table>
		

	</div>
	<div style="clear: both;
    display: block;
    height: 4rem;"></div>			
	
<!-- footer starts here -->	
<?php $this->load->view('blog/footer');?>
<!-- footer ends here -->

<script>
$(document).ready(function (){
    //fill data
    var btnedit='';
    var btndelete = '';
        fillpevents();
        // add data
        $("#addevent").submit(function (e){
            e.preventDefault();
            $("#loader").show();
            var url = $(this).attr('action');
            var data = $(this).serialize();
            $.ajax({
                url:url,
                type:'POST',
                data:data
            }).done(function (data){
                $("#response").html(data);
                $("#loader").hide();
                fillpevents();
            });
        });
    
    function fillpevents(){
        $("#loader").show();
        $.ajax({
            url:'<?php echo base_url() ?>blog/fillpevents',
            type:'GET'
        }).done(function (data){
            $("#fillpevents").html(data);
            $("#loader").hide();
            btnedit = $("#fillpevents .btnedit");
            btndelete = $("#fillpevents .btndelete");
            var deleteurl = btndelete.attr('href');
            var editurl = btnedit.attr('href');
            //delete record
            btndelete.on('click', function (e){
                e.preventDefault();
                var deleteid = $(this).data('past_event_id');
                if(confirm("Are you sure you want to delete this?")){
                    $("#loader").show();
                    $.ajax({
                    url:deleteurl,
                    type:'POST' ,
                    data:'past_event_id='+deleteid
                    }).done(function (data){
                    $("#response").html(data);
                    $("#loader").hide();
                    fillpevents();
                    });
                }
            });
            
            //edit record
            btnedit.on('click', function (e){
                e.preventDefault();
                var editid = $(this).data('past_event_id');
                $.colorbox({
                href:"<?php echo base_url()?>blog/edit/"+editid,
                top:50,
                width:500,
                onClosed:function() {fillpevents();}
                });
            });
            
        });
    }
    
});
</script>