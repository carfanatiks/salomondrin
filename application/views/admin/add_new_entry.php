<?php $this->load->view('blog/header_admin');?>
	
	<div class="col-md-3">
		<!-- column-two -->
		<?php $this->load->view('blog/menu_sidebar');?>	
	</div>
  
	<div class="col-md-8">
		<h2>Add New Entry</h2>
		<hr>
		<div class="well">
			<?php echo form_open('add-new-entry');?>			
			<?php if(validation_errors()){echo validation_errors('<div class="alert alert-danger">','</p>'.'</div>');} ?>
		    <?php if($this->session->flashdata('message')){echo '<div class="alert alert-success">'.$this->session->flashdata('message').'</div>';}?>
			<div class="form-group">
				<label>Title:</label>
				<input class="form-control" type="text" name="entry_name" size="30" />
			</div>
            <div class="form-group">
                <label>Friendly URL:</label>
                <input class="form-control" type="text" name="friend_url" size="100" />
            </div>  			
			<!--h3>Category</h3>
			<p><?php if( isset($categories) && $categories): foreach($categories as $category): ?>
				<label><input class="checkbox" type="checkbox" name="entry_category[]" value="<?php echo $category->category_id;?>"><?php echo $category->category_name;?></label>
				<?php endforeach; else:?>
				Please add your category first!
				<?php endif; ?>
			</p-->
			<div class="form-group">
				<label>Your Entry:</label>
				<textarea rows="16" cols="80%" name="entry_body" id="mytext" style="resize:none;"></textarea>
			</div>
			<div class="form-group">
				<label for="image">Thumb image to show at home (from Images Library):</label>
				<input class="form-control" type="text" name="entry_thumb" id="entry_thumb" maxlength="255" placeholder="Url"/>
	        	
	    	</div>	
			
			<input class="btn btn-success" type="submit" value="Submit"/>
			<!--input class="button" type="reset" value="Reset"/ -->	
		
			</form>
		</div>
		<h2>Images Library</h2>
            <hr>
            <div id="response"></div>
            <table class="table">
                <thead>
                    <tr><th>Image</th><th>Link URL</th><th align="center">Actions</th></tr>
                </thead>
                <tbody id="fillimages">
                    

                </tbody>
                <tfoot></tfoot>
            </table>

	</div>
	<div style="clear: both;
    display: block;
    height: 4rem;"></div>			
	


	<!-- footer starts here -->	
	<?php $this->load->view('blog/footer');?>
	<!-- footer ends here -->

<script>
$(document).ready(function (){
    //fill data
    var btnedit='';
    var btndelete = '';
        fillimages();
        // add data

    
    function fillimages(){
        $("#loader").show();
        $.ajax({
            url:'<?php echo base_url() ?>blog/fillimages',
            type:'GET'
        }).done(function (data){
            $("#fillimages").html(data);
            $("#loader").hide();
            btnedit = $("#fillimages .btnedit");
            btndelete = $("#fillimages .btndelete");
            var deleteurl = btndelete.attr('href');
            var editurl = btnedit.attr('href');
            //delete record
            btndelete.on('click', function (e){
                e.preventDefault();
                var deleteid = $(this).data('id_up');
                if(confirm("Are you sure you want to delete this?")){
                    $("#loader").show();
                    $.ajax({
                    url:deleteurl,
                    type:'POST' ,
                    data:'id_up='+deleteid
                    }).done(function (data){
                    $("#response").html(data);
                    $("#loader").hide();
                    fillimages();
                    });
                }
            });
            
            //edit record
            btnedit.on('click', function (e){
                e.preventDefault();
                var editid = $(this).data('id_up');
                $.colorbox({
                href:"<?php echo base_url()?>blog/edit/"+editid,
                top:50,
                width:500,
                onClosed:function() {fillimages();}
                });
            });
            
        });
    }
    
});
</script>