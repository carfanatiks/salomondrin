<?php $this->load->view('blog/header_admin');?>
	
	<div class="col-md-3">
		<!-- column-two -->
		<?php $this->load->view('blog/menu_sidebar');?>	
	</div>
	<div class="col-md-8">
		<h2>Add New Event</h2>
		<hr>
		<div class="well">
			<?php echo form_open('add-new-event');?>		
			<?php if(validation_errors()){echo validation_errors('<p class="error">','</p>');} ?>
		    <?php if($this->session->flashdata('message')){echo '<p class="success">'.$this->session->flashdata('message').'</p>';}?>
			
			<div class="form-group">
				<label>Event Name:</label>
				<input class="form-control"  type="text" name="event_name" size="30" />
			</div>
			<div class="form-inline">
				<div class="form-group">
					<label>Date:</label>
					<input class="form-control"  type="text" name="event_date" size="30" />
				</div>
				<div class="form-group">
					<label>Place:</label>
					<input class="form-control"  type="text" name="event_place" size="30" />
				</div>
			</div><br>
			<div class="form-group">	
				<label for="image">Image:</label>
	        	<input type="file" name="event_image" id="event_image" placeholder="Choose an image to upload" maxlength="255" />
	    	</div>
	    	<hr>	
			
	        <br>

			<input class="btn btn-success" type="submit" value="Submit"/>
			<!--input class="button" type="reset" value="Reset"/-->	
		
			</form>
		</div>

	</div>
	<div style="clear: both;
    display: block;
    height: 4rem;"></div>			
	


	<!-- footer starts here -->	
	<?php $this->load->view('blog/footer');?>
	<!-- footer ends here -->
