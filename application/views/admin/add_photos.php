<?php $this->load->view('blog/header_admin');?>
	
	<div class="col-md-3">
		<!-- column-two -->
		<?php $this->load->view('blog/menu_sidebar');?>	
	</div>
	<div class="col-md-8">
		<h2>Add a new image</h2>
		<hr>
		<div class="well">
            <form id="addphoto" name="photo" action="<?php echo base_url(); ?>add-photos" method="POST" enctype="multipart/form-data">
			
			
			<?php //if(validation_errors()){echo validation_errors('<div class="alert alert-danger">','</p>'.'</div>');} ?>
		    <?php if($this->session->flashdata('message')){echo '<div class="alert alert-success">'.$this->session->flashdata('message').'</div>';}?>

			<div class="form-group">
				<label for="image">Upload a new picture:</label>
	        	<input type="file" name="image_up" id="image_up" placeholder="Choose an image to upload" maxlength="255" />
	    	</div>				
			<input class="btn btn-success" type="submit" value="Submit"/>
			<!--input class="button" type="reset" value="Reset"/ -->	
		
			</form>
				
		</div>
		<h2>Images Library</h2>
			<hr>
            <div id="response"></div>
			<table class="table">
	            <thead>
	            	<tr><th>Image</th><th>Link URL</th><th align="center">Actions</th></tr>
	            </thead>
	            <tbody id="fillimages">
	            	

	            </tbody>
	            <tfoot></tfoot>
	        </table>

	</div>
	<div style="clear: both;
    display: block;
    height: 4rem;"></div>			

	<!-- footer starts here -->	
	<?php $this->load->view('blog/footer');?>
	<!-- footer ends here -->
<script>
$(document).ready(function (){
    //fill data
    var btnedit='';
    var btndelete = '';
        fillimages();
        // add data  
    function fillimages(){
        $("#loader").show();
        $.ajax({
            url:'<?php echo base_url() ?>blog/fillimages',
            type:'GET'
        }).done(function (data){
            $("#fillimages").html(data);
            $("#loader").hide();
            btnedit = $("#fillimages .btnedit");
            btndelete = $("#fillimages .btndelete");
            var deleteurl = btndelete.attr('href');
            var editurl = btnedit.attr('href');
            //delete record
            btndelete.on('click', function (e){
                e.preventDefault();
                var deleteid = $(this).data('id_up');
                if(confirm("Are you sure you want to delete this?")){
                    $("#loader").show();
                    $.ajax({
                    url:deleteurl,
                    type:'POST' ,
                    data:'id_up='+deleteid
                    }).done(function (data){
                    $("#response").html(data);
                    $("#loader").hide();
                    fillimages();
                    });
                }
            });
            
            //edit record
            btnedit.on('click', function (e){
                e.preventDefault();
                var editid = $(this).data('id_up');
                $.colorbox({
                href:"<?php echo base_url()?>blog/edit/"+editid,
                top:50,
                width:500,
                onClosed:function() {fillimages();}
                });
            });
            
        });
    }
    
});
</script>