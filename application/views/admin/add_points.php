<?php $this->load->view('blog/header_admin');?>
	
	<div class="col-md-3">
		<!-- column-two -->
		<?php $this->load->view('blog/menu_sidebar');?>	
	</div>
	<div class="col-md-8">
		<h2>Add Cupon</h2>
		<hr>
		<div class="well">
            <form id="addphoto" name="photo" action="<?php echo base_url(); ?>cupons" method="POST" enctype="multipart/form-data">
			
			
			<?php //if(validation_errors()){echo validation_errors('<div class="alert alert-danger">','</p>'.'</div>');} ?>
		    <?php if($this->session->flashdata('message')){echo '<div class="alert alert-success">'.$this->session->flashdata('message').'</div>';}?>

            <div class="form-group">
                <label for="cupon">Cupon:</label>
                <input type="text" name="cupon" id="cupon" placeholder="Cupon" maxlength="255" />
            </div>
             <div class="form-group">
                <label for="name">Value:</label>
                <input type="text" name="pp_value" id="pp_value" placeholder="Value" maxlength="255" />
            </div>

            <div class="form-group" id="sandbox-container">
                <label for="validto">Valid To:</label>
                <input type="text" name="validto" id="validto" placeholder="Valid To" maxlength="255" />
            </div>
            <div class="form-group" id="sandbox-container">
                <label for="uses">Uses:</label>
                <input type="text" name="uses" id="uses" placeholder="Uses" maxlength="255" />
            </div>

			
			<input class="btn btn-success" type="submit" value="Submit"/>
			<!--input class="button" type="reset" value="Reset"/ -->	
		
			</form>
				
		</div>
        <h2>Products</h2>
            <hr>
            <div id="response"></div>
            <table class="table">
                <thead>
                    <tr><th>Cupon</th><th>Value</th><th>Expiration</th><th>Uses</th></tr>
                </thead>
                <tbody id="fillimages">
                    

                </tbody>
                <tfoot></tfoot>
            </table>
		

	</div>
	<div style="clear: both;
    display: block;
    height: 4rem;"></div>			

	<!-- footer starts here -->	
	<?php $this->load->view('blog/footer');?>
	<!-- footer ends here -->
<script>
$(document).ready(function (){
 $(function() {
    $( "#validto" ).datepicker();
  });
  
    //fill data
    var btnedit='';
    var btndelete = '';
        fillimages();
        // add data  
    function fillimages(){
        $("#loader").show();
        $.ajax({
            url:'<?php echo base_url() ?>points/fillcupons',
            type:'GET'
        }).done(function (data){
            $("#fillimages").html(data);
            $("#loader").hide();
           
            
            
        });
    }
    
});
</script>