<?php $this->load->view('blog/header_admin');?>
	
	<div class="col-md-3">
		<!-- column-two -->
		<?php $this->load->view('blog/menu_sidebar');?>	
	</div>
    
	<div class="col-md-8">

		<h2>Edit Entries</h2>
			<hr>
            <div id="response"></div>
			<table class="table">
	            <thead>
	            	<tr><th>Title</th><th>Thumbnail image</th><th>Date</th><th align="center">Actions</th></tr>
	            </thead>
	            <tbody id="fillentries">	            	

	            </tbody>
	            <tfoot></tfoot>
	        </table>

	</div>
	<div style="clear: both;
    display: block;
    height: 4rem;"></div>			
	


	<!-- footer starts here -->	
	<?php $this->load->view('blog/footer');?>
	<!-- footer ends here -->

<script>
$(document).ready(function (){
    //fill data
    var btnedit='';
    var btndelete = '';
        fillentries();
        
    function fillentries(){
        $("#loader").show();
        $.ajax({
            url:'<?php echo base_url() ?>blog/fillentries',
            type:'GET'
        }).done(function (data){
            $("#fillentries").html(data);
            $("#loader").hide();
            
            btndelete = $("#fillentries .btndelete");
            var deleteurl = btndelete.attr('href');
            var editurl = btnedit.attr('href');
            //delete record
            btndelete.on('click', function (e){
                e.preventDefault();
                var deleteid = $(this).data('entry_id');
                if(confirm("Are you sure you want to delete this?")){
                    $("#loader").show();
                    $.ajax({
                    url:deleteurl,
                    type:'POST' ,
                    data:'entry_id='+deleteid
                    }).done(function (data){
                    $("#response").html(data);
                    $("#loader").hide();
                    fillentries();
                    });
                }
            });
            
            //edit record
            btnedit.on('click', function (e){
                e.preventDefault();
                var editid = $(this).data('entry_id');
                $.colorbox({
                href:"<?php echo base_url()?>blog/edit/"+editid,
                top:50,
                width:500,
                onClosed:function() {fillentries();}
                });
            });
            
        });
    }
    
});
</script>
