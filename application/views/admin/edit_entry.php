<?php $this->load->view('blog/header_admin');?>
	
	<div class="col-md-3">
		<!-- column-two -->
		<?php $this->load->view('blog/menu_sidebar');?>	
	</div>
	<div class="col-md-8">
		<h2>Edit Entry</h2>
		<hr>
		<div class="well">        
			<?php echo form_open('/blog/edit_entry/'.$entry_id);?>
            <?php foreach($query->result() as $post):?> 
                
        			<div class="form-group">
        				<label>Title:</label>
        				<input class="form-control" type="text" name="entry_name" size="30" value="<?php echo $post->entry_name?>" />
        			</div>
                    <div class="form-group">
                        <label>Friendly URL:</label>
                        <input class="form-control" type="text" name="friend_url" size="100" value="<?php echo $post->sef?>"/>
                    </div> 			
        			<div class="form-group">
        				<label>Your Entry:</label>
        				<textarea rows="16" cols="80%" name="entry_body" id="mytext" style="resize:none;">
                            <?php echo $post->entry_body;
?>
                        </textarea>
        			</div>
        			<div class="form-group">
        				<label for="image">Thumb image to show at home (from Images Library):</label>
        				<input class="form-control" type="text" name="entry_thumb" id="entry_thumb" maxlength="255" placeholder=" E.g. 10_post.jpg" value="<?php echo $post->entry_thumb;
?>"/>
        	        	
        	    	</div>	
        			<input type="hidden" name="hidden" value="<?php echo $post->entry_id;
?>"/>
        			<input class="btn btn-success" type="submit" value="Save changes"/>
        			<!--input class="button" type="reset" value="Reset"/ -->	
        		   
            <?php endforeach; ?>
    		</form>
		</div>


        <h2>Images Library</h2>
            <hr>
            <table class="table">
                <thead>
                    <tr><th>Image</th><th>Link URL</th><th align="center">Actions</th></tr>
                </thead>
                <tbody id="fillimages">
                    

                </tbody>
                <tfoot></tfoot>
            </table>


	</div>
	<div style="clear: both;
    display: block;
    height: 4rem;"></div>			
	


	<!-- footer starts here -->	
	<?php $this->load->view('blog/footer');?>
	<!-- footer ends here -->
<script>
    $(document).ready(function (){

        fillimages();

        $("#frmupdate").submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'<?php echo base_url() ?>blog/update',
                type:'POST',
                dataType:'json',
                data: $("#frmupdate").serialize()
            }).done(function (data){
                window.mydata = data;
                    if(mydata['error'] !=""){
                        $(".errorresponse").html(mydata['error']);
                    }
                    else{
                    $(".errorresponse").text('');
                    $('#frmupdate')[0].reset();
                    $("#response").html(mydata['success']);
                    
                    $.colorbox.close();
                    $("#response").html(mydata['success']);
                    }
            });
        });


        function fillimages(){
        $("#loader").show();
        $.ajax({
            url:'<?php echo base_url() ?>blog/fillimages',
            type:'GET'
        }).done(function (data){
            $("#fillimages").html(data);
            $("#loader").hide();
            btnedit = $("#fillimages .btnedit");
            btndelete = $("#fillimages .btndelete");
            var deleteurl = btndelete.attr('href');
            var editurl = btnedit.attr('href');
            //delete record
            btndelete.on('click', function (e){
                e.preventDefault();
                var deleteid = $(this).data('id_up');
                if(confirm("Are you sure you want to delete this?")){
                    $("#loader").show();
                    $.ajax({
                    url:deleteurl,
                    type:'POST' ,
                    data:'id_up='+deleteid
                    }).done(function (data){
                    $("#response").html(data);
                    $("#loader").hide();
                    fillimages();
                    });
                }
            });
            
            //edit record
            btnedit.on('click', function (e){
                e.preventDefault();
                var editid = $(this).data('id_up');
                $.colorbox({
                href:"<?php echo base_url()?>blog/edit/"+editid,
                top:50,
                width:500,
                onClosed:function() {fillimages();}
                });
            });
            
        });
    }
            
    });   
</script>
