<div class="well">
    <div class="errorresponse">        
    </div>
    <form class="form" id="frmupdate" role="form" action="<?php echo base_url() ?>blog/update" method="POST">
        <?php foreach($query->result() as $row):?>
                            
            <div class="form-group">
            <label for="exampleInputEmail2">Event Name</label>
            <input type="text" name="past_event_name" class="form-control" value="<?php echo $row->past_event_name?>">
            </div>
            <div class="form-group">
                <label >Review Link</label>
                <input type="text" class="form-control" name="past_event_name" value="<?php echo $row->past_event_link?>">
            </div>
            <div class="form-group">
                <input type="hidden" name="hidden" value="<?php echo $past_event_id?>">
                <input type="submit" class="btn btn-success"  value="update">
            </div>
        <?php endforeach;?>
    </form>
</div>
</div>

<script>
    $(document).ready(function (){
        $("#frmupdate").submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'<?php echo base_url() ?>blog/update',
                type:'POST',
                dataType:'json',
                data: $("#frmupdate").serialize()
            }).done(function (data){
                window.mydata = data;
                    if(mydata['error'] !=""){
                        $(".errorresponse").html(mydata['error']);
                    }
                    else{
                    $(".errorresponse").text('');
                    $('#frmupdate')[0].reset();
                    $("#response").html(mydata['success']);
                    
                    $.colorbox.close();
                    $("#response").html(mydata['success']);
                    }
            });
        });    
    });   
</script>