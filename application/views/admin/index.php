<?php $this->load->view('blog/header');?>

		<div class="col-md-4">
			<!-- column-two -->
			<?php $this->load->view('blog/menu_sidebar');?>	
		</div>
		<div class="col-md-8">
			<h2>Dashboard</h2>
			<hr>
			<h3>My Profile</h3>
			
			<p>Username: <?php echo $user->username;?></p>
			<p>Email: <?php echo $user->email;?></p>
			<p>Last login: <?php echo unix_to_human($user->last_login);?></p>
		</div>

	<!-- footer starts here -->	
	<?php $this->load->view('blog/footer');?>
	<!-- footer ends here -->
