<html class="no-js" lang="en"> 
<meta charset="utf-8" />
<head>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SALOMONDRIN</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/><!-- to work in responsive -->
    <meta property="og:title" content="Salmondrin">
    <meta property="og:description" content=''>
    <meta property="og:url" content=''>
    <meta property="og:image" content=''>

    <!-- Bootstrap Core CSS -->
    <link href="http://www.salomondrin.com/css/bootstrap.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="http://www.salomondrin.com/css/styles.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://www.salomondrin.com/css/transitions.css" media="all" />
    

    <!--[if lt IE 9]>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <![endif]-->
    <!--[if (gte IE 9) | (!IE)]><!-->
    
</head>
<body>


    <div id="fb-root"></div>


    <header class="header">
         <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="http://www.salomondrin.com/images/logo_salomondrin.png"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
    
        <!-- /.navbar-collapse -->
    </nav>
    </header>
    <!-- Post Content -->
    <div class="container">
    <h1>Congratulations!<br>You just ordered <?php echo $product['productName']; ?> successfully.</h1>
  	<hr>
    <div class="row">
         <div class="img">
                    <img class="img-responsive" src="<?php echo base_url(); ?><?php echo $product['url']; ?>" alt=""></div>
     </div>
     <br /><br />
     <div class="col-md-12" align="center">
        <a href="<?php echo base_url(); ?>" class="btn btn-default"  >
            Back Home
        </a>
     </div>
     <div class="clearfix"></div>

      <hr>

 </div>
</div>

<hr>
    <!-- /.container -->


<!-- footer -->
    <footer class="navbar-fixed-bottom">
        <ul>
            <li>All rights reserved Salomondrin 2015.</li>
            <li><a href="">Contact</a></li>
        </ul>
    </footer>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.js"></script>

</body>
</html>