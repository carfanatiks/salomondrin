<?php $this->load->view('blog/header');?>
		
	<div class="container">
        
        <div class="row">

    <div class="col-xs-12 col-md-12">
      <div class="main">
      
      <h3>Log In / Sign Up</h3>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <a href="/auth/loginfacebook/" class="btn btn-lg btn-primary btn-block">Facebook</a>
        </div>

      </div>
      <div class="login-or">
        <hr class="hr-or">
        <span class="span-or">or</span>
      </div>
      <?php if($this->session->flashdata('message')){echo '<p class="success">'.$this->session->flashdata('message').'</p>';}?>
      <div id="infoMessage"><?php echo $message;?></div>		
			<?php echo form_open("auth/login");?>	
        <div class="form-group">
				<label for="identity">Username/Email:</label><br>
				<?php echo form_input($identity);?>
			  </div>	  
			  <div class="form-group">
				<label for="password">Password:</label><br>
				<?php echo form_input($password);?>
			  </div>
        <div class="checkbox pull-right">
          <label>
            <?php echo form_checkbox('remember', '1', FALSE);?>
            Remember me </label>
        </div>
        <button type="submit" class="btn btn btn-success">
          Log In
        </button>

         <button type="button"  onclick="location.href='/auth/create_user';" class="btn btn btn-primary">
          Register
        </button>
      </form>
    
    </div>
    </div>
    
  </div>
        <!-- /.row -->
    </div>

<?php $this->load->view('blog/footer');?>
