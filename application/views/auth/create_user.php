<?php $this->load->view('blog/header');?>


<div class="col-md-4 col-md-offset-3">

	<div class="pageTitle" style><h3>Create User</h3></div>
    <hr>
	<div class="well">
        <?php if($this->session->flashdata('message')){echo '<p class="success">'.$this->session->flashdata('message').'</p>';}?>
        <div class="pageTitleBorder"></div>
            <p>Please enter the user information below.</p>
            
        <div id="infoMessage"><?php echo $message;?></div>
        
        <?php echo form_open("auth/create_user");?>
        
        <div class="form-group">
            <label for="identity">Username:</label><br>
        
        <?php echo form_input($identity);?>
        </div>

        <div class="form-group">
            <label for="first_name">First Name:</label><br>
        
        <?php echo form_input($first_name);?>
        </div>
        
        <div class="form-group">
            <label for="last_name">Last Name:</label><br>
        
        <?php echo form_input($last_name);?>
        </div>
        
        <div class="form-group">
            <label for="email">Email:</label><br>
        
        <?php echo form_input($email);?>
        </div>
        
        <!--p>Phone:<br />
        <?php //echo form_input($phone1);?>
        </p-->
        <div class="form-group">
            <label for="password">Password:</label><br>
        
        <?php echo form_input($password);?>
        </div>
        
        <div class="form-group">
            <label for="password_confirm">Confirm Password:</label><br>
        <?php echo form_input($password_confirm);?>
        </div>
        
        <div class="form-group">
        <?php echo form_submit('submit', 'Create User', 'class="btn btn-success"');?>
        </div>
        
    </div>
        
        <?php echo form_close();?>

    </div>

<?php $this->load->view('blog/footer');?>
