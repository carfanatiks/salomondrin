<?php $this->load->view('blog/header');?>
<body>
	<!-- Post Content -->
    <div class="container events">
        <div class="row">
            <!-- Blog Post Content Column -->
            <div class="col-lg-12">
                <!-- Title -->
                <h1>Coming Events</h1>
                <hr>
                <div class="next-events">
                    <!-- COMING EVENTS SLIDER -->
                    <div id="content-slider-1" class="royalSlider contentSlider rsDefault">                        
                        <?php if( $events ): $i=1; foreach($events as $event): ?>
                        <div><!-- EACH EVENT -->
                            
                            <img class="rsImg" src="<?php echo base_url().'uploads/'.$event->event_image;?>" />
                            <h3><?php echo ($event->event_name);?></h3>
                            <span class="info-event"><!-- infor for event -->
                                <p>
                                    <b>Date:</b>
                                    <?php echo ($event->event_date);?>
                                </p>
                                <p>
                                    <b>Place:</b>
                                    <?php echo ($event->event_place);?>
                                </p>
                            </span>
                            <span class="rsTmb"><?php echo ($i++); ?></span>  
                        
                        </div>
                         <?php endforeach; else: ?>
                        <h4>No new events on the list yet!</h4>
                    <?php endif;?>           
                    </div>
                               
                    
                </div>
                 <!-- Title -->
                <h1>Past Events</h1> 
                <hr>
                <ul class="past-events">
                    <?php if( $past_events ): foreach($past_events as $past_event): ?>
                        <li><a href="<?php echo ($past_event->past_event_link);?>" target="_blank"><?php echo ($past_event->past_event_name);?></a></li>

                    <?php endforeach; else: ?>
                        <h4>No past events in the list yet!</h4>
                    <?php endif;?>        
                </ul> 
                
            </div>

        </div>
        <!-- /.row -->
    </div>

<!-- footer starts here -->	
<?php $this->load->view('blog/footer');?>
<!-- footer ends here -->

 <script id="addJS">jQuery(document).ready(function($) {
      // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
      // it's recommended to disable them when using autoHeight module
      $('#content-slider-1').royalSlider({
        autoHeight: true,
        arrowsNav: true,
        fadeinLoadedSlide: false,
        controlNavigationSpacing: 0,
        controlNavigation: 'tabs',
        imageScaleMode: 'none',
        imageAlignCenter:false,
        loop: false,
        loopRewind: true,
        numImagesToPreload: 6,
        keyboardNavEnabled: true,
        usePreloader: false
      });
    });
    </script>