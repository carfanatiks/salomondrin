<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

<title><?php if( isset($title) && $title !== NULL ) { echo $title;} ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1.0"/><!-- to work in responsive -->

<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<!-- Open Graph meta tags -->
<meta property="fb:app_id" content="802566159859922" />
<meta property="og:site_name" content="Salomondrin - Cupgang"/>
<meta name="author" content="Salomondrin" />
<meta name="designer" content="Ana Villegas" />
<meta property="og:description" content='#CUPGANG is more than camaraderie. It’s thicker than blood. The bond we share with each other is indestructible.'>
<meta property="og:url" content='<?php echo base_url(); ?>'>
<meta property="og:image" content='<?php echo base_url(); ?>images/cg.png'>
<link rel="shortcut icon" type="image/ico" href="<?php echo base_url(); ?>favicon.ico">

<!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>css/bootstrap2.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/bootstrap-social.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/login.css" media="all" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/styles2.css" media="all" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/transitions.css" media="all" />

  	<!--[if lt IE 9]>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <![endif]-->
    <!--[if (gte IE 9) | (!IE)]><!-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <!--<![endif]-->

    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!--home grid posts -->
    <script src="<?php echo base_url(); ?>js/jquery.collagePlus.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.removeWhitespace.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.collageCaption.js"></script>
    <script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.js"></script>
     <script type="text/javascript">

     $(window).load(function () {
            $(document).ready(function(){
                collage();
                $('.Collage').collageCaption();

            });
        });

        // All images need to be loaded for this plugin to work so
        // we end up waiting for the whole window to load in this example
        
        // Here we apply the actual CollagePlus plugin
        function collage() {
            $('.Collage').removeWhitespace().collagePlus(
                {
                    'fadeSpeed'     : 2000,
                    'targetHeight'  : 380,
                    'effect'        : 'effect-2',
                    'direction'     : 'vertical',
                    'allowPartialLastRow':true
                }
            );
        };

        

        // This is just for the case that the browser window is resized
        var resizeTimer = null;
        $(window).bind('resize', function() {
            // hide all the images until we resize them
            $('.Collage .Image_Wrapper').css("opacity", 0);
            // set a timer to re-apply the plugin
            if (resizeTimer) clearTimeout(resizeTimer);
            resizeTimer = setTimeout(collage, 200);
        });
    </script>
</head>
<body class="home">
    <header class="header">
         <!-- Navigation -->
	    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	        <!-- Brand and toggle get grouped for better mobile display -->
	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	            <a class="navbar-brand" href="#"><img src="<?php echo base_url(); ?>images/logo_salomondrin.png"></a>
	        </div>
	        <!-- Collect the nav links, forms, and other content for toggling -->
	        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	            <ul class="nav navbar-nav navbar-right">
                   <?php
                    $user = $this->ion_auth->user()->row();
                     if( $this->ion_auth->logged_in() ) // block un-authorized access
                     {

                    ?>
                    <li>
                        <a href="<?php echo base_url(); ?>users/<?php echo $this->session->userdata('user_id'); ?>"><?php echo $user->first_name; ?></a>
                    </li>
                    


                    <?php } ?>

                <li>
                  <a href="<?php echo base_url(); ?>cupgang">Cupgang</a>
                </li>
                <!--li>
                  <a href="<?php echo base_url(); ?>events">Events</a>
                </li-->
                <li>
                  <a href="<?php echo base_url(); ?>about">About</a>
                </li>
                <li>
                  <a href="<?php echo base_url(); ?>store">Store</a>
                </li>
               <?php if($this->ion_auth->logged_in() ){ ?>
                    <li>
                        <a href="<?php echo base_url(); ?>auth/logout">Logout</a>
                    </li>

               <?php } else {  ?>
               <li>
                        <a href="<?php echo base_url(); ?>auth/login">Log In</a>
                    </li>

                <?php } ?>
	                
	                <li class="social">
                        <a href="https://www.youtube.com/user/asalomondrin" target="_blank"><img src="<?php echo base_url(); ?>images/yt.png" target="_blank"></a>
	                    <a href="https://instagram.com/salomondrin/" target="_blank"><img src="<?php echo base_url(); ?>images/icon_instagram.png"></a>
	                    <a href="https://www.facebook.com/Salomondrin777?fref=ts" target="_blank"><img src="<?php echo base_url(); ?>images/icon_facebook.png"></a>
	                    <a href="https://twitter.com/salomondrin" target="_blank"><img src="<?php echo base_url(); ?>images/icon_twitter.png"></a>
	                </li>
	            </ul>
	        </div>
	        <!-- /.navbar-collapse -->
	    </nav>
    </header>