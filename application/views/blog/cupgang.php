<?php $this->load->view('blog/header');?>
<body>

	<!-- Post Content -->
    <div class="container post about">
        <div class="row">
            <!-- Blog Post Content Column -->
            <div class="col-lg-12">
                <!-- Title -->
                <h1>#CUPGANG</h1>
                <hr>
                <!-- TOP CUPGANG -->
                  <div class="col-md-6 col-md-offset-3">
                    <div class="top-post image"><!-- when the top of the post its a video -->
                        <img class="img-responsive" src="images/cg.png" alt="">
                    </div>
                  </div>
                  <div class="col-md-6 col-md-offset-3">
                    <div class="text">
                        <p><b>#CUPGANG</b> is more than camaraderie. It’s thicker than blood. The bond we share with each other is indestructible. we've taken different roads to become who we are but it doesn’t matter where you’ve been, it matters who you’re with and when you’re with us, you’re family. We stand united. We stand strong. We stand together.</p>

                    </div>
                  </div>


                
            </div>

        </div>
        <!-- /.row -->
    </div>
    <div style="clear: both;
    display: block;
    height: 4rem;"></div>   

<!-- footer starts here -->	
<?php $this->load->view('blog/footer');?>
<!-- footer ends here -->