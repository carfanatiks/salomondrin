
	<?php if ( ! $this->ion_auth->logged_in() ):?>
	<h3>Admin Menu</h3>
	<ul class="sidemenu">
		<li><a href="<?php echo base_url().'auth/login';?>">Login</a></li>
	<?php else: ?>
	<div class="nav nav-stacked" id="sidebar">
		<h3>Admin Menu</h3>
		<ul class="sidemenu nav">
			<li><a href="<?php echo base_url().'auth/';?>">Dashboard</a></li>
			<li><a href="<?php echo base_url().'add-new-entry';?>">Add new entry</a></li>
			<li><a href="<?php echo base_url().'add-photos';?>">Images library</a></li>
			<li><a href="<?php echo base_url().'all-entries';?>">Edit Entries</a></li>
			<li><a href="<?php echo base_url().'products';?>">Add Products</a></li>
			<li><a href="<?php echo base_url().'cupons';?>">Add Cupons</a></li>
			<li><a href="<?php echo base_url().'purchases';?>">Purchases</a></li>
			<!--li><a href="<?php echo base_url().'add-new-event';?>">Next Events Gallery</a></li>
			<li><a href="<?php echo base_url().'add-past-event';?>">Past Events List</a></li-->
			<!--li><a href="<?php echo base_url().'add-new-category';?>">Add new category</a></li-->
			<li><a href="<?php echo base_url().'auth/logout';?>">Logout</a></li>
		<?php endif; ?>
			
		</ul>

	</div>

<script type="text/javascript">
	/* activate sidebar */
$('#sidebar').affix({
  offset: {
    top: 235
  }
});
</script>