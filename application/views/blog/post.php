<?php $this->load->view('blog/header');?>

	<!-- Post Content -->
    <div class="container post">
    	<div class="row">
    		<!-- Blog Post Content Column -->
            <div class="col-lg-12">
            	<?php if( $query ): foreach($query as $post): ?>
            	<!-- Title -->
                <h1><?php echo ucwords($post->entry_name);?></h1>
                <!-- Author -->
                <p class="lead">
                	by <a href="#"><?php $author = $this->ion_auth->user($post->author_id)->row(); echo ucfirst($author->username);?></a>  &nbsp;&nbsp;
                    <span class="glyphicon glyphicon-time"></span> Posted on <span class="date"><?php echo mdate('%d %M, %Y %H:%i %a',human_to_unix($post->entry_date));?></span>	
                </p>
                <hr>
            </div>
            <!-- Blog Post Content Column END-->
    	</div>
				

			<!-- Post txt Content -->
            <div class="text">
            	<?php echo $post->entry_body;
?>
            </div>
            <!-- fb Comments -->
            <div class="fb-comments" data-href="<?php echo base_url().'post/'.$post->entry_id;?>" data-width="100%" data-numposts="6"></div> 
               
			<?php endforeach; ?>
			<?php endif;?>

	</div>		
	<!-- Post Content END-->			
			
<div style="clear: both;
    display: block;
    height: 4rem;"></div>   


	<!-- footer starts here -->	
	<?php $this->load->view('blog/footer');?>
	<!-- footer ends here -->
<script type="text/javascript">
    $( "p:has(iframe)" ).addClass( "video-container" );
    $( "p:has(img)" ).addClass( "image-container" );
</script>
</script>
</body>
</html>