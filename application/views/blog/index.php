<?php $this->load->view('blog/header_home');?>
	
	<section class="Collage effect-parent" id="postswrapper">

		
	<?php if( $posts ): foreach($posts as $post): 
        
        $entry_thumb = '';

        $mystring = $post->entry_thumb;
       
       
           $entry_thumb = $post->entry_thumb;
        

       
        ?>
		<div class="Image_Wrapper" onclick="location.href='<?php echo base_url().'post/'.$post->sef;?>';" data-caption="<?php echo ucwords($post->entry_name);?>">
            <a href="<?php echo base_url().'post/'.$post->sef;?>">
            	<img  src="<?php echo $post->entry_thumb;?>" />
            </a>
        </div>	
			<?php endforeach; else: ?>
			<h2>No post yet!</h2>
		<?php endif;?>
	</section>		


<script type="text/javascript">
$(window).scroll(function()
{
    if($(window).scrollTop() == $(document).height() - $(window).height())
    {
    	var numItems = $('.Image_Wrapper').length;
    	
        console.log(numItems);
       

        
        $.ajax({
        url: "/blog/paginated/"+numItems,
        success: function(html)
        {
            if(html)
            {
                $("#postswrapper").append(html);

                $('#postswrapper').imagesLoaded()
                  .always( function( instance ) {
                    console.log('all images loaded');
                  })
                  .done( function( instance ) {
                    console.log('all images successfully loaded');
                    collage();
                    $('.Collage').collageCaption();

                    
                  })
                  .fail( function() {
                    console.log('all images loaded, at least one is broken');
                  })
                  .progress( function( instance, image ) {
                    var result = image.isLoaded ? 'loaded' : 'broken';
                    console.log( 'image is ' + result + ' for ' + image.img.src );
                  });

               
				
            
            }else
            {
                $('div#loadmoreajaxloader').html('<center>No more posts to show.</center>');
            }

            
                	
        }
        });


    }
});


</script>


	<!-- footer starts here -->	
	<?php $this->load->view('blog/footer');?>


	<!-- footer ends here -->
