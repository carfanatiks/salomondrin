<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

<title><?php if( isset($title) && $title !== NULL ) { echo $title;} ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1.0"/><!-- to work in responsive -->

<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="author" content="Salomondrin" />
<meta property="og:description" content=''>
<meta property="og:url" content=''>
<meta property="og:image" content=''>
<link rel="shortcut icon" type="image/ico" href="<?php echo base_url(); ?>favicon.ico">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/styles.css" media="all" />
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">



<!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet">  
    <link href="<?php echo base_url('assets/datatables/css/dataTables.bootstrap.css')?>" rel="stylesheet">
     <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/transitions.css" media="all" />

    <link href="http://cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.4.33/example1/colorbox.min.css" rel="stylesheet"/>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
     
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.4.33/jquery.colorbox-min.js"></script>


  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">

  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <style>#loader{display: none}</style>

  	<!--[if lt IE 9]>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <![endif]-->
    <!--[if (gte IE 9) | (!IE)]><!-->
   
    <!--<![endif]-->

    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/tiny_mce/tiny_mce.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>js/tiny_mce/plugins/media/js/embed.js"></script>

    <script type="text/javascript">
      tinyMCE.init({
        // General options
        selector: "#mytext",
        theme : "advanced",
        document_base_url : "<?php echo base_url(); ?>",
        relative_urls : false,
        extended_valid_elements : "iframe[src|frameborder|style|scrolling|class|width|height|name|align]",
        
        plugins : "autolink,lists,pagebreak,layer,save,advhr,advimage,advlink,iespell,inlinepopups,insertdatetime,media,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",

        // Theme options
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,hr,|,media,image",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,help,code,|,insertdate",
        
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        content_css : "<?php echo base_url(); ?>css/tiny_mce/content.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "<?php echo base_url(); ?>js/tiny_mce/template_list.js",
        external_link_list_url : "<?php echo base_url(); ?>js/tiny_mce/link_list.js",
        external_image_list_url : "<?php echo base_url(); ?>js/tiny_mce/image_list.js",
        media_external_list_url : "<?php echo base_url(); ?>js/tiny_mce/media_list.js",

        

        // Replace values for the template plugin
        template_replace_values : {
          username : "Some User",
          staffid : "991234"
        }
      });
</script>
    <!-- /TinyMCE -->

<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>
    
</head>
<body>
  
    <header class="header">
         <!-- Navigation -->
	    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	        <!-- Brand and toggle get grouped for better mobile display -->
	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	            <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>images/logo_salomondrin.png"></a>
	        </div>
	        <!-- Collect the nav links, forms, and other content for toggling -->
	        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	            <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo base_url(); ?>cupgang">Cupgang</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>store">Store</a>
                    </li>


                    <li>
                        <a href="<?php echo base_url(); ?>about">About</a>
                    </li>
	                <li class="social">
	                    <a href="https://instagram.com/salomondrin/" target="_blank"><img src="<?php echo base_url(); ?>images/icon_instagram.png" target="_blank"></a>
	                    <a href="https://www.facebook.com/Salomondrin777?fref=ts"><img src="<?php echo base_url(); ?>images/icon_facebook.png"></a>
	                    <a href="https://twitter.com/salomondrin" target="_blank"><img src="<?php echo base_url(); ?>images/icon_twitter.png"></a>
	                </li>
	            </ul>
	        </div>
	        <!-- /.navbar-collapse -->
	    </nav>
    </header>