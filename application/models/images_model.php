<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Images_model extends CI_Model {

	function getImages()
	{
		$this->db->select('u.id_up, u.image_up');
		$this->db->from('upload as u');
		
		
		$data = $this->db->get();
		$resultData = array();
        foreach ($data->result() as $row){
        	$resultData[] = array(
        		'id' => $row->id_up,
        		'image_up' => $row->image_up

        		);    
        }

        return $resultData;

	}

	public function edit_upload($id, $thumb){
		$data = array(

			'image_up'	=> $thumb
		);
		$this->db->where('id_up', $id);
		$this->db->update('upload',$data);
		
		return $object_id = (int) mysql_insert_id(); // get latest post id
	}

		function getImageBody($name = null)
	{
		$this->db->select('e.entry_id, e.entry_body');
		$this->db->from('entry as e');
		$this->db->like('e.entry_body', $name);

		
		$data = $this->db->get();
		$resultData = array();
        foreach ($data->result() as $row){
        	$resultData[] = array(
        		'entry_id' => $row->entry_id,
        		'entry_body' => htmlentities ($row->entry_body)

        		);    
        }

        return $resultData;

	}

	function getImageThumb($name = null)
	{
		$this->db->select('e.entry_id, e.entry_thumb');
		$this->db->from('entry as e');
		$this->db->like('e.entry_thumb', $name);

		
		$data = $this->db->get();
		$resultData = array();
        foreach ($data->result() as $row){
        	$resultData[] = array(
        		'entry_id' => $row->entry_id,
        		'entry_thumb' => $row->entry_thumb

        		);    
        }

        return $resultData;

	}



	public function edit_entry($id, $body){
		$data = array(

			'entry_body'	=> html_entity_decode($body)
		);
		$this->db->where('entry_id', $id);
		$this->db->update('entry',$data);
		
		return $object_id = (int) mysql_insert_id(); // get latest post id
	}

	public function edit_entry_thumb($id, $thumb){
		$data = array(

			'entry_thumb'	=> $thumb
		);
		$this->db->where('entry_id', $id);
		$this->db->update('entry',$data);
		
		return $object_id = (int) mysql_insert_id(); // get latest post id
	}



}