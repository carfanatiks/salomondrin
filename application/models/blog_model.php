<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Blog_model extends CI_Model {

	function get_posts()
	{
		$this->db->order_by('entry_date','desc'); // get all entry, sort by latest to oldest
		$this->db->limit(35);
		$query = $this->db->get('entry');

		return $query->result();
	}

	function get_paginated_posts($start, $page)
	{
		$this->db->order_by('entry_date','desc'); // get all entry, sort by latest to oldest
		$this->db->limit($page, $start);
		$query = $this->db->get('entry');
		 log_message('error', "QUERY:". $this->db->last_query());
		return $query->result();
	}
	function get_photos()
	{
		$this->db->order_by('id_up','desc'); // get all photos, sort by latest to oldest
		$query = $this->db->get('upload');
		return $query->result();
	}
	function get_events()
	{
		$this->db->order_by('event_id','asc'); // get all events, sort by latest to oldest
		$query = $this->db->get('event');
		return $query->result();
	}
	function get_past_events()
	{
		$this->db->order_by('past_event_id','desc'); // get all events, sort by latest to oldest
		$query = $this->db->get('past_event');
		return $query->result();
	}
	function all_entries()
	{
		$this->db->order_by('entry_id','desc'); // get all events, sort by latest to oldest
		$query = $this->db->get('entry');
		return $query->result();
	}
	
/* LOADING all ENTRIES */
	public function fillentries(){
		$this->db->order_by("entry_date", "desc"); 
        $data = $this->db->get('entry');
        foreach ($data->result() as $row){
            $edit = base_url().'blog/edit_entry/'."$row->entry_id";
            $delete = base_url().'blog/delete_entry/'."$row->entry_id";
            echo "<tr>
                    <td>$row->entry_name</td>
                    <td><img src='$row->entry_thumb' style='max-width:120px;'/></td>
                    <td>$row->entry_date</td>                    
                    	<td><a href='$edit' data-id='$row->entry_id' class='btnedit' title='edit'><i class='glyphicon glyphicon-pencil' title='edit'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='$delete' class='btndelete' data-id='$row->entry_id'  title='delete'><i class='glyphicon glyphicon-remove'></i></a>
                        </td>    
                   </tr>";
            
        }
        exit;
    }


	function add_new_entry($author,$name,$body,$categories,$thumb, $sef)
	{

		$data = array(
			'author_id'		=> $author,
			'entry_name'	=> $name,
			'entry_body'	=> html_entity_decode($body),
			'entry_thumb'	=> $thumb,
			'sef'			=> $sef
		);
		$this->db->insert('entry',$data);
		
		$object_id = (int) mysql_insert_id(); // get latest post id
		
		//foreach($categories as $category)
		//{
		//	$relationship = array(
		//		'object_id'		=> $object_id, // object id is post id
		//		'category_id'	=> $category,
		//	);
		//	$this->db->insert('entry_relationships',$relationship);
		//}
	}
	public function edit_entry($id, $author,$name,$body,$categories,$thumb, $sef){
		$data = array(
			'author_id'		=> $author,
			'entry_name'	=> $name,
			'entry_body'	=> html_entity_decode($body),
			'entry_thumb'	=> $thumb,
			'sef'			=> $sef
		);
		$this->db->where('entry_id', $id);
		$this->db->update('entry',$data);
		
		$object_id = (int) mysql_insert_id(); // get latest post id
	}	
	public function delete_entry($table, $where = array()) {
	  $this->db->where($where);
	  $res = $this->db->delete($table); 
	  if($res)
	    return TRUE;
	  else
	    return FALSE;
	}


/* LOADING IMAGES LIBRARY */
	public function fillimages(){
		$this->db->order_by("id_up", "desc"); 
        $data = $this->db->get('upload');
        foreach ($data->result() as $row){
        	$url = $row->image_up;
            $edit = base_url().'blog/edit/';
            $delete = base_url().'blog/delete_img/'."$row->id_up";
            echo "<tr>
                    <td><img src='$url' style='max-width:150px;'/></td>
                    <td>$url</td>
                    	<td><a href='$delete' data-id='$row->id_up' class='btndelete' title='delete'><i class='glyphicon glyphicon-remove'></i></a>
                        </td>    
                   </tr>";
            
        }
        exit;
    }


/* LOADING IMAGES LIBRARY */
	public function fillproducts(){
		$this->db->select('products.idProduct, products.name, products.description,  images.url');
		$this->db->from('products');
		$this->db->join('image_products as images', 'products.idProduct = images.idProduct');
		$this->db->order_by("idProduct", "desc"); 
		


		
        $data = $this->db->get();
        foreach ($data->result() as $row){
        	$url = base_url() ."$row->url";
        	$name = $row->name;
            
            $delete = base_url().'points/delete_prod/'."$row->idProduct";
            echo "<tr>
                    <td><img src='$url' style='max-width:150px;'/></td>
                    <td>$name</td>
                    	<td><a href='$delete' data-id='$row->idProduct' class='btndelete' title='delete'><i class='glyphicon glyphicon-remove'></i></a>
                        </td>    
                   </tr>";
            
        }
        exit;
    }



public function fillcupons(){
		$this->db->select('points.cupon, points.value, points.validTo, points.uses, points.global');
		$this->db->from('points');
		
		$this->db->order_by("idpoint", "desc"); 
		


		
        $data = $this->db->get();
        foreach ($data->result() as $row){
        	
        	$name = $row->cupon;
            $value = $row->value;
            $valid = $row->validTo;
            $uses = $row->uses.'/'.$row->global;
            $delete = base_url().'points/delete_prod/'."$row->idProduct";
            echo "<tr>
                    
                    <td>$name</td>
                    <td>$value</td>
                    <td>$valid</td>
                    <td>$uses</td>      
                   </tr>";
            
        }
        exit;
    }


 
    function insertPennis(array $dataInsert, $history_points, $totalUses){

    	$this->db->insert('user_points', $dataInsert);
    	


    	$insertedUserPoints =  $this->db->insert_id();

    	$data = array('uses' => $totalUses);
		$this->db->where('idpoint', $dataInsert['id_points']);
		$this->db->update('points', $data);


		
		$historyData = array('history_points' => $history_points);
		$this->db->where('id', $dataInsert['id_user']);
		$this->db->update('users', $historyData);

		return $insertedUserPoints;





    }

    function updatePennis($dataUpdate = array(), $userId){

    	$this->db->where('id', $userId);
		$this->db->update('users', $dataUpdate); 

    }

    function notUsed($idCupon, $idUser){
    	$this->db->select('cupon');
    	$this->db->from('user_points');
    	$this->db->where('id_points', $idCupon);
    	$this->db->where('id_user', $idUser);
    	$data = $this->db->get();
    	$i = 0;
        foreach ($data->result() as $row){
        	$i++;
        }

        if ($i > 0){
        	return false;
        } else {
        	return true;
        }

    }

    function checkCupon($cupon){
    	$this->db->select('idpoint, cupon, value, uses');
    	$this->db->from('points');
    	$this->db->where('cupon', $cupon);
    	$this->db->where('uses >', 0);
    	$data = $this->db->get();
    	$i = 0;
        foreach ($data->result() as $row){
        	$i++;
        	$result = array(
        		'idpoint' => $row->idpoint,
        		'value' => $row->value,
        		'uses' => $row->uses
        		);

        }

        if ($i > 0){
        	return $result;
        } else {
        	return false;
        }

    }

/* add IMAGES to LIBRARY */
	function add_photos($image)
	{
		$data = array(
			'image_up'	=> $image,
		);
		$this->db->insert('upload',$data);
		
		$object_id = (int) mysql_insert_id(); // get latest post id
		
	}

	function add_products(array $data){
		

		
		$this->db->insert('products', $data);

		return $this->db->insert_id();

	}




	function add_cupons(array $data){
		

		
		$this->db->insert('points', $data);

		return $this->db->insert_id();

	}

	function add_product_image(array $data){
		
		$this->db->insert('image_products', $data);

		return $this->db->insert_id();

	}

	function add_imgprods($image)
	{
		$data = array(
			'image_up'	=> $image,
		);
		$this->db->insert('upload',$data);
		
		$object_id = (int) mysql_insert_id(); // get latest post id
		
	}
	public function delete_img($table, $where = array()) {
	  $this->db->where($where);
	  $res = $this->db->delete($table); 
	  if($res)
	    return TRUE;
	  else
	    return FALSE;
	}

	public function delete_prod($table, $where = array()) {
	  $this->db->where($where);
	  $res = $this->db->delete($table); 
	  if($res)
	    return TRUE;
	  else
	    return FALSE;
	}

/* CREATE NEW EVENT ON ADMIN */
	function add_new_event($ename,$edate,$eplace,$eimage)
	{
		$data = array(
			'event_name'	=> $ename,
			'event_date'	=> $edate,
			'event_place'	=> $eplace,
			'event_image'	=> $eimage,
		);
		$this->db->insert('event',$data);
		
		$object_id = (int) mysql_insert_id(); 
	}
/* LOADING PAST EVENTS TABLE IN ADMIN */
	public function fillpevents(){
		$this->db->order_by("past_event_id", "desc"); 
        $data = $this->db->get('past_event');
        foreach ($data->result() as $row){
            $edit = base_url().'blog/edit/';
            $delete = base_url().'blog/delete/';
            echo "<tr>
                    <td>$row->past_event_name</td>
                    <td>$row->past_event_link</td>
                    	<td><a href='$edit' data-id='$row->past_event_id' class='btnedit' title='edit'><i class='glyphicon glyphicon-pencil' title='edit'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='$delete' data-id='$row->past_event_id' class='btndelete' title='delete'><i class='glyphicon glyphicon-remove'></i></a>
                        </td>    
                   </tr>";
            
        }
        exit;
    }
	function add_past_event($pastename,$pastelink)
	{

		$data = array(
			'past_event_name'	=> $pastename,
			'past_event_link'	=> $pastelink,
			
		);
		$this->db->insert('past_event',$data);		
		$object_id = (int) mysql_insert_id(); 
	}
	



	function add_new_comment($post_id, $commentor, $email, $comment, $user_id)
	{
		$total_count = 0;
		
		$data = array(
			'entry_id'		=> $post_id,
			'comment_name'	=> $commentor,
			'comment_email'	=> $email,
			'comment_body'	=> $comment,
			'user_id'		=> $user_id,
		);
		$this->db->insert('comment',$data);
		
		$query = $this->get_post($post_id);
		
		foreach($query as $post)
		{
			$total_count = $post->comment_count + 1;
		}
		
		$count = array(
			'comment_count'	=> $total_count,
		);
		
		$this->db->where('entry_id',$post_id);
		$query = $this->db->update('entry',$count); // update comment count

	}
	
	function get_id_by_sef($sef){
		$this->db->where('sef',$sef);
		$query = $this->db->get('entry');
		if($query->num_rows()!==0)
		{
			$result = $query->result_array();
            return $result[0];
		}
		else
			return FALSE;
	}

	function get_post($id)
	{
		$this->db->where('entry_id',$id);
		$query = $this->db->get('entry');
		if($query->num_rows()!==0)
		{
			return $query->result();
		}
		else
			return FALSE;
	}
	
	function get_post_comment($post_id)
	{
		$this->db->where('entry_id',$post_id);
		$query = $this->db->get('comment');
		return $query->result();
	}
	
	function total_comments($id)
	{
		$this->db->like('entry_id', $id);
		$this->db->from('comment');
		return $this->db->count_all_results();
	}
	
	function add_new_category($name,$slug)
	{
		$i = 0;
		$slug_taken = FALSE;
		
		while( $slug_taken ==  FALSE ) // to avoid duplicate slug
		{
			$category = $this->get_category(NULL,$slug);
			if( $category == FALSE )
			{
				$slug_taken = TRUE;
				$data = array(
					'category_name'	=> $name,
					'slug'			=> $slug,
				);
				$this->db->insert('entry_category',$data);
			}
			$i = $i + 1;
			$slug = $slug.'-'.$i;
		}
	}
	
	function get_category($id = FALSE, $slug)
	{
		if( $id != FALSE)
			$this->db->where('category_id',$id);
		elseif( $slug )
			$this->db->where('slug',$slug);
		
		$query = $this->db->get('entry_category');
		
		if( $query->num_rows() !== 0 )
		{
			return $query->result();
		}
		else
			return FALSE; // return false if no category in database
	}
	
	function get_categories()
	{
		$query = $this->db->get('entry_category'); 
		return $query->result();
	}
	
	function get_related_categories($post_id)
	{
		$category = array();
		
		$this->db->where('object_id',$post_id);
		$query = $this->db->get('entry_relationships'); // get category id related to the post
		
		foreach($query->result() as $row)
		{
			$this->db->where('category_id',$row->category_id);
			$query = $this->db->get('entry_category'); // get category details
			$category = array_merge($category,$query->result());
		}
		
		return $category;
	}
	
	function get_category_post($slug)
	{
		$list_post = array();
		
		$this->db->where('slug',$slug);
		$query = $this->db->get('entry_category'); // get category id
		if( $query->num_rows() == 0 )
			show_404();
		
		foreach($query->result() as $category)
		{
			$this->db->where('category_id',$category->category_id);
			$query = $this->db->get('entry_relationships'); // get posts id which related the category
			$posts = $query->result();
		}
		
		if( isset($posts) && $posts )
		{
			foreach($posts as $post)
			{
				$list_post = array_merge($list_post,$this->get_post($post->object_id)); // get posts and merge them into array
			}		
		}
		
		return $list_post; // return an array of post object
	}
}

/* End of file blog_model.php */
/* Location: ./application/models/blog_model.php */