<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Products_model extends CI_Model {

	function get_products()
	{
		$this->db->select('p.idProduct, p.name as productName, p.description, p.pp_value,  images.url');
		$this->db->from('products as p');
		$this->db->join('image_products as images', 'p.idProduct = images.idProduct');
		$this->db->order_by("priority", "asc"); 
		
		$data = $this->db->get();
		$resultData = array();
        foreach ($data->result() as $row){
        	$resultData[] = array(
        		'idProduct' => $row->idProduct,
        		'productName' => $row->productName,
        		'description' => $row->description,
        		'pp_value' => $row->pp_value,
        		'url' => $row->url

        		);    
        }

        return $resultData;

	}

		function get_product($id = null)
	{
		$this->db->select('p.idProduct, p.name as productName, p.description, p.pp_value,  images.url');
		$this->db->from('products as p');
		$this->db->join('image_products as images', 'p.idProduct = images.idProduct');
		$this->db->where('p.idProduct', $id);
		
		$data = $this->db->get();
		$resultData = array();
        foreach ($data->result() as $row){
        	$resultData = array(
        		'idProduct' => $row->idProduct,
        		'productName' => $row->productName,
        		'description' => $row->description,
        		'pp_value' => $row->pp_value,
        		'url' => $row->url

        		);    
        }

        return $resultData;

	}

	function purchase($dataUpdate = array()){
		$this->db->insert('purchases', $dataUpdate);

	}

}